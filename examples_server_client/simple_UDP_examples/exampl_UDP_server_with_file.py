"""Пример, когда порт не используется в качестве точки обмена. В качестве точки обмена используется файл"""

import os
import socket

unix_sock_name = 'unix_sock'  # сохраняем сокет в качестве констаныБ чтобы не копипастить
sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)  # создаем экземляр класса сокет, но с AF_UNIX - это значит
#  что используем отдельный вид сокета, не основанный на IP протоколеБ а используем  UNIX - сокет,
#  который использует точку обмена файл

if os.path.exists(unix_sock_name):  # проверяем, создан ли уже этот файл. Если да, то
    os.remove(unix_sock_name)       # удаляем его

sock.bind(unix_sock_name)  # если файл не был создан, резервирем этот файл, указываем имя файла

while True:
    try:
        result = sock.recv(1024)
    except KeyboardInterrupt:
        sock.close()
        break
    else:
        print('Message ', result.decode('utf-8'))