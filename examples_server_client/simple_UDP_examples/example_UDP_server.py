"""example UDP server socket, который получает одно сообщение"""

import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # создаем экземпляр класса socket
sock.bind(('127.0.0.1', 8888))  # используем метод bind для резервирования порта на нашей локальной машине
result = sock.recv(1024)  # пытаемся принять сообщение из сети в размере 1024 байта
print('Message ', result.decode('utf-8'))  # декодируем сообщение и распечатываем
sock.close()  # не забывать закрывать порт после использования
