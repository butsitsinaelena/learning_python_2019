from xmlrpc.server import SimpleXMLRPCServer


def is_even(n):
    return n % 2 == 0


if __name__ == '__main__':
    server = SimpleXMLRPCServer(("localhost", 8000))  # создаем экземпляр SimpleXMLRPCServer
    # и задаем адре хоста и порт, который он будет прослушивать
    print("Listening on port 8000...")
    server.register_function(is_even, "is_even")  # регистрируем функцию для удаленного вызова, чтоб
    # сервер знал, как ее вызывать
    server.serve_forever()  # запускаем сервер в бесконечном цикле ожидания и обработки запросов
