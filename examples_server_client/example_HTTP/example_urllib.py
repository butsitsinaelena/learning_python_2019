"""Очень удобная библиотека"""
from urllib import request

response = request.urlopen('http://example.com')  # - урл адрес, можно передавать ip-адрес

print(response.status)
print(response.getcode())
print(response.msg)
print(response.reason)
# получаем заголовок в виде внутреннего обьекта
print(response.headers)
# получаем словарь всех заголовков
print(response.getheaders())
# получение заголовка
print(response.headers.get('Content-Type'))
print(response.getheader('Content-Type'))