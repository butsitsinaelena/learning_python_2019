import socket


def parse_http_response(text_response):  # функция которая парсит. принимает текст и разбивает его на опр. части
    lines = text_response.split('\n')
    status_raw, lines = lines[0], lines[1:]
    protocol, status_code, message = status_raw.split(' ')
    empty_index = 1
    headers = {}
    for index, line in enumerate(lines):
        line = line.strip()
        line = line.strip('\r')
        if line == '':
            empty_index = index
            break
        print(line)
        k, _, v = line.partition(':')
        headers.setdefault(k.strip(), v.strip())
    content = ''.join(lines[empty_index + 1:])
    return int(status_code), headers, content


sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # создаем TCP сокет
sock.connect(('example.com', 80))  # подклучаемся к серверу
content_items = [
    'GET / HTTP/1.1',  # ормируем тело запроса
    'Host: example.com',
    'Connection: keep-alive',
    'Accept: text/html',
    '\n'
]
content = '\n'.join(content_items)
print('---HTTP MESSAGE---')
print(content)
print('--- END OF MESSAGE---')
sock.send(content.encode())  # отправляем запрос
result = sock.recv(10024)  # принимаем ответ от сервера
status_code, headers, content = parse_http_response(result.decode())  # пытаемся распарсить ответ
print('Status_cod:{}'. format(status_code))
print('Headers:{}'.format(headers))
print('Content:')
print(content)


