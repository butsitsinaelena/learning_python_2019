print('________________Bracket validator___________________')

""" Программа проверяет правильность растановки скобок. Наборы скобок для анализа беруться из тестового файла.
    После анализа правильности растановки скобок, программа сравнивает результаты, полученные в процессе ее 
    работы(True, False), с результатами указанными в тестовом файле. После этого выдает значение PASS - если результаты
    совпадают, значение FAIL - если резултаты не совпадают
"""
import sys
sys.path.append('/home/test/python2019')
from homework_check_bracket.check_bracket_def import check_bracket
from homework_check_bracket.check_bracket_def import compare_value


if __name__ == '__main__':
    with open('check_bracket.txt', 'r') as f:
        num_tests = 0
        num_passed = 0
        print('{:20s} | {!s:8} | {!s:6} | {:8} |'.format('Brackets String', 'Expected', 'Actual', 'Compare'))
        print('---------------------+----------+--------+--------+')

        for line in f.readlines():
            line = line.rstrip()
            bracket_string, expected_result = line.split()
            expected_result = (expected_result == 'True')
            actual_result = check_bracket(bracket_string)
            comparison_result = compare_value(actual_result, expected_result)
            passed = expected_result == actual_result
            if passed:
                num_passed += 1
            num_tests += 1
            print('{:20s} | {!s:8} | {!s:6} | {:8} |'.format(bracket_string, expected_result, actual_result, comparison_result))

        num_failed = num_tests - num_passed
        all_passed = num_tests == num_passed
        print('---------------------+----------+--------+--------+')
        print('Overall result:                          | {:8} |'.format(compare_value(actual_result, expected_result)))
        print('-----------------------------------------+--------+')
        print('All: {}   Passed: {}   Failed: {}'.format(num_tests, num_passed, num_failed))