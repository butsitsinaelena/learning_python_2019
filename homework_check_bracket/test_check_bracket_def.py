from check_bracket_def import check_bracket
from check_bracket_def import compare_value
import unittest


class TestBrackets(unittest.TestCase):
    def setUp(self):
        print('start')

    def test_normal_bracket(self):
        self.assertTrue(check_bracket('()'))
        self.assertTrue(check_bracket('()()'))

    def test_incorrect_bracket(self):
        self.assertFalse(check_bracket('('))
        self.assertFalse(check_bracket(')'))
        self.assertFalse(check_bracket('(()'))
        self.assertFalse(check_bracket('()('))
        self.assertFalse(check_bracket('())'))

    def test_pass(self):
        self.assertEqual(compare_value(True, True), 'PASS')
        self.assertEqual(compare_value(False, False), 'PASS')

    def test_fail(self):
        self.assertEqual(compare_value(True, False), 'FAIL')
        self.assertEqual(compare_value(False, True), 'FAIL')

    def tearDown(self):
        print('end')


if __name__ == '__main__':
    unittest.main()
