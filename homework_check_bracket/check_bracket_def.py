# check_bracket_def
def check_bracket(bracket):
    """
    Функция, которая проверяет правильность расстановки скобочек в строке
    :param bracket: строка
    :return: True - если скобочки расставлены верно
                False - если скобочки расставлены не верно
    """
    counter_bracket = []
    len_bracket = len(bracket)
    for i in range(len_bracket):
        if bracket[i] == '(':
            counter_bracket.append(1)
        elif bracket[i] == ")":
            if counter_bracket == []:
                return False
            else:
                counter_bracket.pop()
    if counter_bracket == []:
        return True
    elif counter_bracket != []:
        return False


def compare_value(expected, actual):
    """
    :param expected: ожидаемый результат работы программыБ взятый из файла
    :param actual: результат, полученный в результате работы программы
    :return: 'PASS' - если оба рузультата равны
                'FAIL' - если результаты отличаются
    """
    if expected == actual:
        return 'PASS'
    else:
        return 'FAIL'
