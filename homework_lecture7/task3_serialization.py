"""Создать класс Human с 5-10 атрибутами (имя, фамилия, возраст, меcто жительства и т.д.). Написать функцию,
которая создавала бы указанное количество экземпляров, сериализовывала их и сохраняла в файл human.data, и
другую функцию, которая бы читала файл human.data, десериализовывала его содержимое и выводила результат на печать.
Примечание: чтоб у экземпляров Human были разные значения атрибутов, можно воспользоваться функциями random.randint()
и random.choice()."""

import pickle
from random import choice, randint


class Human:
    def __init__(self, name, last_name, age, hometown):
        self.name = name
        self.last_name = last_name
        self.age = age
        self.hometown = hometown

    def __repr__(self):
        return ('My name is {}, last name is {}, age {}, i was born in {}'.format(self.name, self.last_name,
                                                                                  self.age, self.hometown))


def create_and_serialization(quant):
    with open('human.data', 'wb') as pf:
        for i, human in enumerate(range(quant), 1):
            list_name = ['Anna', 'Nik', 'Inga', 'Natasha', 'Alex', 'Ivan']
            list_last_name = ['Red', 'Black', 'Pitt', 'Ivanov', 'Fisher']
            list_town = ['NY', 'Moscow', 'Paris', 'Rim']
            name = choice(list_name)
            last_name = choice(list_last_name)
            age = randint(12, 65)
            town = choice(list_town)
            human = Human(name, last_name, age, town)
            pick = pickle.dumps((i, human))
            pf.write(pick)


def deserialization(f_name, quant):
    list_unpick_human = []
    with open(f_name, 'rb') as prf:
        for line in range(quant):
            unpick_human = pickle.load(prf)
            list_unpick_human.append(unpick_human)
            print(unpick_human)


if __name__ == '__main__':
    quantity = int(input('Enter quantity human for serialization '))
    create_and_serialization(quantity)
    deserialization('human.data', quantity)
