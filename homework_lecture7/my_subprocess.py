"""С помощью библиотеки subprocess прочитать содержимое
произвольного файла с использованием утилиты cat в Linux (имя файла
должно передаваться как параметр в вашу функцию)."""

from subprocess import Popen, PIPE


proc = Popen(['ls', '-l', '/tmp'], stdout=PIPE, stderr=PIPE)
proc.wait() # дождаться выполнения
res = proc.communicate() # получить tuple('stdout', 'stderr')
if proc.returncode:
    print(res[1])
print('result:', res[0])