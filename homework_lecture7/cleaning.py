"""Написать программу, которая уничтожает файлы и папки по истечении
заданного времени. Вы указываете при запуске программы путь до
директории, за которой нашему скрипту необходимо следить. После запуска
программа не должна прекращать работать, пока вы не остановите ее
работу с помощью Ctrl+C (подсказка: для постоянной работы программы
необходим вечный цикл, например, "while True:", при нажатии Ctrl+C
автоматически остановится любая программа). Программа следит за
объектами внутри указанной при запуске папки и удаляет их тогда, когда
время их существования становится больше одной минуты для файлов и
больше двух минуты для папок (то есть дата создания отличается от
текущего момента времени больше чем на одну/две минуты). Ваш скрипт
должен смотреть вглубь указанной папки. Например, если пользователь
создаст внутри нее папку, внутри нее еще одну, а внутри этой какой-то
файл, то этот файл должен удалиться первым (так как файлу положено жить
только одну минуту, а папкам две). Вам понадобятся библиотеки os и shutil.
Внимательно перечитайте задание и учтите возможные ошибки."""
import os
import datetime
import time
import sys


def delete_old_files(folder):
    global total_deleted_file
    for dirpath, dirnames, filenames in os.walk(folder):  #бъект-генератор, из которого получают кортежи
    # для каждого каталога переданной файловой иерархии. dirpath - это строка, путь к каталогу. dirnames это список
    # имена подкаталогов в dirpath (исключая '.' и '..'). filenames - это список имен не-каталогов файлов в dirpath.
        for file in filenames:
            file_name = os.path.join(dirpath, file)  # полный путь до файла
            file_time = os.path.getctime(file_name)  # время создания файла в секундах
            if file_time < time_old_file:
                total_deleted_file += 1
                print('Delete file: ', file_name)
                os.remove(file_name)  # удаление файла


def delete_old_dir(folder):
    global total_deleted_dirs
    for dirpath, dirnames, filenames in os.walk(folder):
        if dirpath != folder:
            if (not filenames) and (not dirnames):  # проверяем есть ли в папке другие файлы или папки
                dir_time = os.path.getmtime(dirpath)  # время создания папки в секундах
                if dir_time < time_old_dir:
                    total_deleted_dirs += 1
                    print('Delete folder: ', dirpath)
                    os.rmdir(dirpath)


if __name__ == '__main__':
    start = datetime.datetime.now()
    total_deleted_file = 0  # общее количества удаленных файлов
    total_deleted_dirs = 0  # общее количествол удаленных папок
    #folder = '/home/test/my_dir'  # путь до директории, за которой скрипту необходимо следить.
    argument = sys.argv
    if len(argument) > 1:
        folder = argument[1]
    else:
        exit(0)
    while True:
        file_life = 60  # максимальное время существования файла, файлы старее будут удалены
        folder_life = 120  # максимальное время существования папки, папки старее будут удалены
        time_now = time.time()
        time_old_file = time_now - file_life
        time_old_dir = time_now - folder_life
        delete_old_files(folder)  # удаляет старые файлы
        delete_old_dir(folder)  # удаляет пустые папки, созданные больше 2 минут назад
        time.sleep(120)
        intermediate_time = datetime.datetime.now() - start
        print('The program runs hour:minute:second.microsecond : ', intermediate_time)
