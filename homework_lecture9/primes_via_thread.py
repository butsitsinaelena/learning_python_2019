"""Написать функцию find_primes(end, start), которая ищет все простые
числа в диапазоне от заданного числа start (по умолчанию 3) до
заданного числа end.
Запустить ее три раза последовательно в диапазоне от 3 до 10000, от
10001 до 20000, от 20001 до 30000 каждый раз в
отдельном потоке с помощью threading.Thread. Что будет, если 'забыть'
выполнить start или join для потоков?"""

import threading
import time


def find_primes(end, start):
    global global_list_primes
    list_primes = []
    if start <= 2:
        start = 2
    if start > end:
        start1 = end
        start, end = start1, start
    for item in range(start, end):
        for denominator in range(2, item):
            if item % denominator == 0:
                break
        else:
            list_primes.append(item)
    print(list_primes)  # print - для обозначения работы потоков
    with mutex:
        global_list_primes.extend(list_primes)
    return global_list_primes


if __name__ == '__main__':
    start = time.time()
    mutex = threading.RLock()
    global_list_primes = []
    threads = []
    thr1 = threading.Thread(target=find_primes, args=(3, 10000))
    thr2 = threading.Thread(target=find_primes, args=(10001, 20000))
    thr3 = threading.Thread(target=find_primes, args=(20001, 30000))
    thr1.start()
    thr2.start()
    thr3.start()
    thr1.join()
    thr2.join()
    thr3.join()
    print('List primes = ', global_list_primes)
    print('Общее время вычислений в секундах: {}'.format(int(time.time() - start)))
    # Общее время вычислений в секундах: от 16 до 19

    # Если забыть выполнить thr.start() у одного потока - то поток не запустится и не выполнит определенную ему
    # работу. Запущенные потоки отработают и программа завершится.
    # Если забыть забыть выполнить thr.start() у всех потоков, то они все не запустятся. Главный поток выполнит свою
    # работу, программа завершится сразу::
    #     List primes =  []
    #     Общее время вычислений в секундах: 0
    #     Process finished with exit code 0

    # Если не выполнить thr.join() одного потока, то видимых изменений не произойдет.Только увеличится время выполнения
    # работы программы. Если не выполнить thr.join() у всех потоков, то главыный поток сразу завершит свою работу:
    #     List primes =  []
    #     Общее время вычислений в секундах: 0
    # Дочерние пооки будут запущены и какое-то время будут продолжать свою работу, но их результаны остануться
    # невостребованными.
    # И потом, когда они завершатся и завершится программа:
    #     Process finished with exit code 0

