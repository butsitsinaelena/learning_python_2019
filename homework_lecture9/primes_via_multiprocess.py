"""Написать функцию find_primes(end, start), которая ищет все простые
числа в диапазоне от заданного числа start (по умолчанию 3) до
заданного числа end. Далее необходимо:
Запустить ее три раза последовательно в диапазоне от 3 до 10000, от
10001 до 20000, от 20001 до 30000, но каждый раз в
отдельном процессе с помощью multiprocessing.Process. Что будет, если
'забыть' выполнить start или join для процессов?"""

import multiprocessing
import time


def find_primes(end, start, child_process):
    list_primes = []
    if start <= 2:
        start = 2
    if start > end:
        start1 = end
        start, end = start1, start
    for item in range(start, end):
        for denominator in range(2, item):
            if item % denominator == 0:
                break
        else:
            list_primes.append(item)
    child_process.send(list_primes)


if __name__ == '__main__':
    start = time.time()
    parent_process_pipe, child_process_pipe = multiprocessing.Pipe()
    process1 = multiprocessing.Process(target=find_primes, args=(3, 10000, child_process_pipe))
    process2 = multiprocessing.Process(target=find_primes, args=(10001, 20000, child_process_pipe))
    process3 = multiprocessing.Process(target=find_primes, args=(20001, 30000, child_process_pipe))
    process1.start()
    process2.start()
    process3.start()
    process1_result = parent_process_pipe.recv()
    process2_result = parent_process_pipe.recv()
    process3_result = parent_process_pipe.recv()
    process1.join()
    process2.join()
    process3.join()
    print(process1_result + process2_result + process3_result)
    print('Общее время вычислений в секундах: {}'.format(int(time.time() - start)))
    # Общее время вычислений в секундах: от 8 до 10

    # Все Process.start() - нет, process.join() есть:
    # ошибок нет. Программа будет долго что-то выполнять и не завершиться.

    # Все Process.start() - есть, process.join() нет:
    # программа отработает, но не за 10, а за 15 секунд.
    # Один process.start() - нет, все process.join() есть:
    # ошибок нет.Программа будет что-то выполнять и не завершится.
    # Один process.start() - нет, все process.join() есть:
    # программа отработает, но не за 10, а за 15 секунд.
