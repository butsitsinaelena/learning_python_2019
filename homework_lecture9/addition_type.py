"""Реализовать запуск функции, осуществляющей операцию сложения для
различных типов (integer, string, list) параллельно с различными
наборами аргументов."""

import threading
from random import choice


def summing(num1, num2):
    try:
        sum = num1 + num2
        print('Sum of value: {} + {} = {}'.format(num1, num2, sum))
    except TypeError:
        print("{} and {} - types is different".format(num1, num2))


if __name__ == '__main__':
    number = int(input('Enter number '))
    threads = []
    for i in range(number):
        list_sum = ['Sasha', 12, 'Masha', [1, 2, 4], 7, [23, 45, 14], ['love', 'toffee'], 111, 5, 'Ivanov']
        value1 = choice(list_sum)
        value2 = choice(list_sum)
        thr = threading.Thread(target=summing, args=(value1, value2))
        thr.start()
        threads.append(thr)

    for thr in threads:
        thr.join()
