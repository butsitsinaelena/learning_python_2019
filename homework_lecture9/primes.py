"""Написать функцию find_primes(end, start), которая ищет все простые
числа в диапазоне от заданного числа start (по умолчанию 3) до
заданного числа end.
Запустить ее три раза последовательно в диапазоне от 3 до 10000, от
10001 до 20000, от 20001 до 30000"""

import time


def find_primes(end, start):
    list_primes = []
    if start <= 2:
        start = 2
    if start > end:
        start1 = end
        start, end = start1, start
    for item in range(start, end):
        for denominator in range(2, item):
            if item % denominator == 0:
                break
        else:
            list_primes.append(item)
    return list_primes


if __name__ == '__main__':
    start = time.time()
    items1 = find_primes(3, 10000)
    items2 = find_primes(10001, 20000)
    items3 = find_primes(20001, 30000)
    print('Общее время вычислений в секундах: {}'.format(int(time.time() - start)))
    sum_items = items1 + items2 + items3
    print(sum_items)
    #Общее время вычислений в секундах: 14

