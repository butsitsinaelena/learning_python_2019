import unittest

testmodules= ['test_class_money', 'test_roman_number']


if __name__ == '__main__':
    suite = unittest.TestSuite()
    for tm in testmodules:
        suite.addTest(unittest.defaultTestLoader.loadTestsFromName(tm))
    unittest.TextTestRunner().run(suite)