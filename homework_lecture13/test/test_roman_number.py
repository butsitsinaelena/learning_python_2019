import unittest
import sys
sys.path.append('/home/test/python2019')
from homework_lecture13.roman_number import to_roman
from homework_lecture13.roman_number import valid_input
from homework_lecture13.roman_number import NonValidInput


class TestRomanNumber(unittest.TestCase):
    def setUp(self):
        print('start')

    def test_ones(self):
        self.assertEqual(to_roman(1), 'I')
        self.assertEqual(to_roman(2), 'II')
        self.assertEqual(to_roman(3), 'III')
        self.assertEqual(to_roman(4), 'IV')
        self.assertEqual(to_roman(5), 'V')
        self.assertEqual(to_roman(6), 'VI')
        self.assertEqual(to_roman(7), 'VII')
        self.assertEqual(to_roman(8), 'VIII')
        self.assertEqual(to_roman(9), 'IX')

    def test_tens(self):
        self.assertEqual(to_roman(10), 'X')
        self.assertEqual(to_roman(20), 'XX')
        self.assertEqual(to_roman(30), 'XXX')
        self.assertEqual(to_roman(40), 'XL')
        self.assertEqual(to_roman(50), 'L')
        self.assertEqual(to_roman(60), 'LX')
        self.assertEqual(to_roman(70), 'LXX')
        self.assertEqual(to_roman(80), 'LXXX')
        self.assertEqual(to_roman(90), 'XC')

    def test_hundreds(self):
        self.assertEqual(to_roman(100), 'C')
        self.assertEqual(to_roman(200), 'CC')
        self.assertEqual(to_roman(300), 'CCC')
        self.assertEqual(to_roman(400), 'CD')
        self.assertEqual(to_roman(500), 'D')
        self.assertEqual(to_roman(600), 'DC')
        self.assertEqual(to_roman(700), 'DCC')
        self.assertEqual(to_roman(800), 'DCCC')
        self.assertEqual(to_roman(900), 'CM')

    @unittest.skipIf(sys.version_info.minor != 7,
                     "not supported in this python version")
    def test_hundreds_plus(self):
        self.assertEqual(to_roman(101), 'CI')
        self.assertEqual(to_roman(203), 'CCIII')

    def test_thousands(self):
        self.assertEqual(to_roman(1000), 'M')
        self.assertEqual(to_roman(2000), 'MM')
        self.assertEqual(to_roman(3000), 'MMM')
        self.assertEqual(to_roman(4000), 'MV')
        self.assertEqual(to_roman(5000), 'V-thousand')

    @unittest.skip("demonstrating skipping")
    def test_foo(self):
        self.assertEqual(to_roman(15), '')

    def test_combine(self):
        self.assertEqual(to_roman(15), 'XV')
        self.assertEqual(to_roman(117), 'CXVII')
        self.assertEqual(to_roman(1224), 'MCCXXIV')

    def test_valid_input(self):
        self.assertRaises(NonValidInput, valid_input, 'b')
        self.assertRaises(NonValidInput, valid_input, -1)
        self.assertRaises(NonValidInput, valid_input, 5001)
        self.assertRaises(NonValidInput, valid_input, 0)
        self.assertEqual(valid_input('10'), 10)
        self.assertEqual(valid_input(10), 10)

    def tearDown(self):
        print('end')


if __name__ == '__main__':
    unittest.main()
