import unittest
import sys
sys.path.append('/home/test/python2019')
from homework_lecture13.class_money import Money
from homework_lecture13.class_money import check_money


class TestClassMoney(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print('start')
        cls.money1 = Money(43, 16)
        cls.money2 = Money(30, 26)
        cls.money3 = Money(0, 16)
        cls.money4 = Money(0, 565)
        cls.money5 = Money(9, 6)
        cls.money6 = Money(9, 6)
        cls.money7 = Money(13, 0)

    def test_penny_count(self):
        self.assertEqual(self.money1.penny_count(), 4316)
        self.assertEqual(self.money2.penny_count(), 3026)
        self.assertEqual(self.money3.penny_count(), 16)
        self.assertEqual(self.money4.penny_count(), 565)
        self.assertEqual(self.money5.penny_count(), 906)
        self.assertEqual(self.money6.penny_count(), 906)
        self.assertEqual(self.money7.penny_count(), 1300)

    def test_init(self):
        self.assertEqual(self.money1.ruble, 43)
        self.assertEqual(self.money1.penny, 16)
        self.assertEqual(self.money2.ruble, 30)
        self.assertEqual(self.money2.penny, 26)
        self.assertEqual(self.money3.ruble, 0)
        self.assertEqual(self.money3.penny, 16)
        self.assertEqual(self.money4.ruble, 0)
        self.assertEqual(self.money4.penny, 565)
        self.assertEqual(self.money5.ruble, 9)
        self.assertEqual(self.money5.penny, 6)
        self.assertEqual(self.money6.ruble, 9)
        self.assertEqual(self.money6.penny, 6)
        self.assertEqual(self.money7.ruble, 13)
        self.assertEqual(self.money7.penny, 0)

    def test_str(self):
        self.assertEqual(str(self.money1),
                         (str(self.money1.ruble) + ' ' + 'руб.' + str(self.money1.penny) + ' ' + 'коп.'))
        self.assertEqual(str(self.money2),
                         (str(self.money2.ruble) + ' ' + 'руб.' + str(self.money2.penny) + ' ' + 'коп.'))

    def test_add(self):
        self.assertEqual(self.money1 + self.money2, Money(73, 42))
        self.assertEqual(self.money2 + self.money3, Money(30, 42))
        self.assertEqual(self.money2 + self.money4, Money(35, 91))
        self.assertEqual(self.money1 + self.money7, Money(56, 16))
        self.assertEqual(self.money4 + self.money7, Money(18, 65))

    def test_sub(self):
        self.assertEqual(self.money1 - self.money2, Money(12, 90))
        self.assertEqual(self.money2 - self.money3, Money(30, 10))
        self.assertEqual(self.money2 - self.money4, Money(24, 61))
        self.assertEqual(self.money1 - self.money7, Money(30, 16))

    def test_lt(self):
        self.assertLess(self.money2, self.money1)
        self.assertLess(self.money3, self.money4)
        self.assertLess(Money(2, 34), Money(0, 567))

    def test_type_error(self):
        self.assertRaises(TypeError, Money, -2, 15)
        self.assertRaises(TypeError, lambda: self.money6 - self.money7)
        self.assertRaises(TypeError, lambda: self.money4 - self.money7)
        self.assertRaises(TypeError, lambda: Money(1, 23) - Money(1, 23))
        self.assertRaises(TypeError, lambda: Money(1, 23) - Money(2, 23))
        self.assertRaises(TypeError, lambda: Money(0, 123) - Money(1, 23))

    def test_check_money(self):
        self.assertEqual(str(check_money(43, 16)), '43 руб.16 коп.')
        self.assertEqual(str(check_money(0, 36)), '0 руб.36 коп.')
        self.assertEqual(str(check_money(0, 146)), '0 руб.146 коп.')

    @classmethod
    def tearDownClass(cls):
        print('end')


if __name__ == '__main__':
    unittest.main()
