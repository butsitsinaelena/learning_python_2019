"""Написать функцию to_roman, которая принимает целое число, а возвращает строку, отображающую это число римскими
цифрами. Например, на вход подается 6, возвращается - "VI"; на вход подается 23, возвращается "XXIII".
Входные данные должны быть в диапазоне от 1 до 5000, если подается число не из этого диапазона, или не число, то
должны выбрасываться ошибка типа NonValidInput. Этот тип ошибки надо создать отдельно. Также необходимо в папке с
файлом, содержащим вашу функцию, создать файл tests.py, внутри которой необходимо определить тесты для вашей функции.
Тесты должны покрывать все возможное поведение функции, включая порождения ошибки при некорректных входных данных."""

"""В римской системе счисления I обозначает 1, V обозначает 5, X — 10, L — 50, C — 100, D — 500, M — 1000.
Например, число 3 в римской системе счисления будет обозначаться как III."""


class NonValidInput(Exception):
    pass


def to_roman(number):
    ones = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]
    tens = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"]
    hundreds = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]
    thousands = ["", "M", "MM", "MMM", "MV", 'V-thousand']

    t = thousands[number // 1000]
    h = hundreds[number // 100 % 10]
    te = tens[number // 10 % 10]
    o = ones[number % 10]

    return t + h + te + o


def valid_input(number):
    try:
        int_number = int(number)
    except Exception:
        raise NonValidInput()
    else:
        if 0 < int_number <= 5000:
            return int_number
        else:
            raise NonValidInput()


if __name__ == '__main__':
    enter_number = input('Enter number= ')
    try:
       correct_number = valid_input(enter_number)
    except NonValidInput:
        print('Out of range or non-integer ')
    except Exception as exc:
        print("Some unexpected error: {}".format(exc))
    else:
        roman_number = to_roman(correct_number)
        print('Roman number = ', roman_number)

