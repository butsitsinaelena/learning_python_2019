"""Написать юнит тесты для класса Money из задания 10 к лекции 7, получить code coverage репорт в html формате."""


class Money:
    def __init__(self, ruble, penny):
        self.ruble = ruble
        self.penny = penny
        if self.ruble < 0 or self.penny < 0:
            raise TypeError()

    def __str__(self):
        return str(self.ruble) + ' ' + 'руб.' + str(self.penny) + ' ' + 'коп.'

    def penny_count(self):
        return self.ruble * 100 + self.penny

    def __add__(self, other):
        amount = self.penny_count() + other.penny_count()
        self.ruble_add = amount // 100
        self.penny_add = amount % 100
        return Money(self.ruble_add, self.penny_add)

    def __sub__(self, other):
        difference = self.penny_count() - other.penny_count()
        if difference <= 0:
            raise TypeError
        else:
            self.ruble_sub = difference // 100
            self.penny_sub = difference % 100
            return Money(self.ruble_sub, self.penny_sub)

    def __lt__(self, other):
        return self.penny_count() < other.penny_count()

    def __eq__(self, other):
        return self.penny_count() == other.penny_count()


def check_money(ruble, penny):
    try:
        money = Money(ruble, penny)
        return money
    except TypeError:
        print("Money cannot be negative")


if __name__ == '__main__':
    m1 = check_money(43, 16)
    print('m1 = {}    его тип {} '.format(m1, type(m1)))
    penny = m1.penny_count()
    print('m1 в копейках = {}    его тип {} '.format(penny, type(penny)))
    m2 = check_money(30, 26)
    print('m2 = ', m2)
    penny2 = m2.penny_count()
    print('m2 в копейках = {}    его тип {} '.format(penny2, type(penny2)))
    m3 = check_money(0, 16)
    print('m3 = ', m3)
    m4 = check_money(0, 565)
    print('m4 = ', m4)
    m5 = check_money(9, 6)
    print('m5 = ', m5)
    m6 = check_money(9, 6)
    print('m6 = ', m6)

    print('\n_________with add_____________')
    m_sum1 = m1 + m2
    print('m_sum1 = ', m_sum1)
    m_sum2 = m2 + m3
    print('m_sum2 = ', m_sum2)
    m_sum3 = m4 + m5
    print('m_sum3 = ', m_sum3)

    print('\n__________with sub_____________')
    try:
        m_subtraction1 = m1 - m2
    except TypeError:
        print('First money less than second money')
    else:
        print('m_subtraction1 = ', m_subtraction1)
    try:
        m_subtraction2 = m2 - m3
    except TypeError:
        print('First money less than second money')
    else:
        print('m_subtraction2 = ', m_subtraction2)
    try:
        m_subtraction3 = m4 - m5
    except TypeError:
        print('First money less than second money')
    else:
        print('m_subtraction3 = ', m_subtraction3)
    try:
        m_subtraction4 = Money(1, 23) - Money(1, 23)
    except TypeError:
        print('First money less than second money')
    else:
        print('m_subtraction4', m_subtraction4)

    print('\n________with comparison_________')

    if m1 == m2:
        print('{} и {} равны  '.format(m1, m2))
    elif m1 > m2:
        print('{} больше чем {}'.format(m1, m2))
    else:
        print('{} меньше чем {}'.format(m1, m2))
    ###########################################
    if m3 == m4:
        print('{} и {} равны  '.format(m3, m4))
    elif m3 > m4:
        print('{} больше чем {}'.format(m3, m4))
    else:
        print('{} меньше чем {}'.format(m3, m4))
    ############################################
    if m5 == m6:
        print('{} и {} равны  '.format(m5, m6))
    elif m3 > m4:
        print('{} больше чем {}'.format(m5, m6))
    else:
        print('{} меньше чем {}'.format(m5, m6))
    #############################################
    if penny == penny2:
        print('{} и {} равны  '.format(penny, penny2))
    elif penny > penny2:
        print('{} больше чем {}'.format(penny, penny2))
    else:
        print('{} меньше чем {}'.format(penny, penny2))

