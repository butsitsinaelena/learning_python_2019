import unittest
import sys


class MyTestCase(unittest.TestCase):
    @unittest.skip("demonstrating skipping")
    def test_nothing(self):
        self.fail("shouldn't happen")

    @unittest.skipIf(sys.version_info.minor != 6,
                     "not supported in this python version")
    def test_format(self):
        # Tests that work for only a certain version of the library.
        pass

    @unittest.skipUnless(sys.platform.startswith("win"), "requires Windows")
    def test_windows_support(self):
        # windows specific testing code
        pass


"""  Ожидаемый результат:
test_format (tests_skip.MyTestCase) ... skipped 'not supported in this
python version'
test_nothing (tests_skip.MyTestCase) ... skipped 'demonstrating skipping'
test_windows_support (tests_skip.MyTestCase) ... skipped 'requires Windows'"""

"""" Мой результат в Run
Skipped: not supported in this python version
Skipped: demonstrating skipping
Skipped: requires Windows
"""

""" Мой результат в terminal
test@test:~/python2019$ python3 -m unittest -v test_skip.py
test_format (test_skip.MyTestCase) ... ok
test_nothing (test_skip.MyTestCase) ... skipped 'demonstrating skipping'
test_windows_support (test_skip.MyTestCase) ... skipped 'requires Windows'

----------------------------------------------------------------------
Ran 3 tests in 0.000s

OK (skipped=2)
"""