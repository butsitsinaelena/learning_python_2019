# Файл test_cmd_manager.py
import exampl_unittests.cmd_manager
import unittest
import mock

class TestCmdManager(unittest.TestCase):
    def test_cmd_manager(self):
        cmd_mgr = exampl_unittests.cmd_manager.CmdManager()

        # Подменяем audit_service mock-объектом
        with mock.patch('cmd_manager.audit_service') as audit_service_mock:
            # Вместо параметра service тоже используем mock-объект
            service = mock.Mock()
            service.name = 'MyService'
            service.run.return_value = 0
            cmd_mgr.set_service(service)
            self.assertTrue(service.start.called)
            cmd_mgr.run_command('status')
            # Используем assert-методы mock-объекта, которым мы подменили audit_service
            audit_service_mock.add_call.assert_called_once_with('MyService', 'run', 'status')
            service.run.assert_called_once_with('status')
            audit_service_mock.add_result.assert_called_once_with('MyService', 'run', 0)


if __name__ == '__main__':
    unittest.main()
