from urllib import request
from urllib.parse import urlparse
from html.parser import HTMLParser


class MyParser(HTMLParser):  # наследуемся от стандартного парсера
    def __init__(self, my_domain):  # передаем домен с которым будем работать
        HTMLParser.__init__(self)  # вызывается конструктор базового класса
        self.output_list = []  # список, в которм будут лежать результаты парсинга
        self.my_domain = my_domain  # адресс

    def handle_starttag(self, tag, attrs):  # переопределяем поведение метода базового класса
        if tag == 'a':  # а - тег для url
            url = dict(attrs).get('href')  # р=href -  атрибут тега а, который содержит сам url
            href_domain = urlparse(url).netloc  # парсим урлы, берем только домен
            if not href_domain:  # если урл не содержит домен
                url = self.my_domain + url  # то формируем урл который содержит домен
            self.output_list.append(url)  # добавляем урл в список


if __name__ == '__main__':
    domain = 'http://google.com'
    req = request.Request(domain)
    response = request.urlopen(req)
    web_page = response.read()
    html = web_page.decode()
    parser = MyParser(domain)
    parser.feed(html)
    for url in parser.output_list:
        status = None
        try:
            req = request.Request(url)
            response = request.urlopen(req)
            web_page = response.read()
            status = 'SUCCESS!'
        except:
            status = 'FAIL'
        print('status = {}, url = {}'.format(status, url))
