"""Написать клиентское и серверное приложения. Клиент отправляет на
сервер список зашифрованных слов, сервер дешифрует слова по
словарю и возвращает клиенту список расшифрованных слов. Клиент
должен вывести полученный список."""

import socket
import json

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # создаем экземпляр класса socket
sock.connect(('', 1224))  # подключаемся к серверу

items = ['I', 'really', 'like', 'coffee', 'apple', 'pears', 'bananas', 'cheeseburgers']
print('Encrypted list: ', items)
json_items = json.dumps(items)  # переводим данные в json формат
sock.send(json_items.encode())  # отправляем сообщение серверу
result = sock.recv(1024).decode()  # получаем ответ сервера и декодируем его
print('Decrypted list: ', result)
sock.close()
