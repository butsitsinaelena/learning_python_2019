import socket
import json

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # создаем экзмпляр класса socket
sock.bind(('127.0.0.1', 1224))
sock.listen(5)   # открываем порт на сервере

while True:
    try:
        client, addr = sock.accept()
    except KeyboardInterrupt:
        sock.close()
        break
    else:
        results = client.recv(1025)
        results = results.decode()
        results_list = json.loads(results)
        results_list_change = []
        list_non = []
        for result in results_list:
            dict = {'I':'Margo',
                    'really':'Marina',
                    'like':'Anton',
                    'coffee':'go away'}
            if result in dict.keys():
                change_word = dict.get(result)
                results_list_change.append(change_word)

        results_change_json = json.dumps(results_list_change)
        client.send(results_change_json.encode())
        client.close()
