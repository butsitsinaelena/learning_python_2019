import socket
import pickle

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # создаем экзмпляр класса
sock.bind(('127.0.0.1', 1225))
sock.listen(5)   # открываем порт на сервере (не более 5 клиентов одновременно)
client_number = 0

while True:
    try:
        client, addr = sock.accept()
    except KeyboardInterrupt:
        sock.close()
        break
    else:
        results_first_processing = client.recv(5025)
        client_number += 1
        results = pickle.loads(results_first_processing)
        print('User number {} - {} :'.format(client_number, results))
        client.close()
