class User:
    def __init__(self, name, last_name, age, weight, height):
        self.name = name
        self.last_name = last_name
        self.age = age
        self.weight = weight
        self.height = height

    def __repr__(self):
        return 'name {}, age {}'.format(self.name, self.age)
