"""Написать клиентское и серверное приложения. Клиент при установке соединения отправляет на сервер информацию
о пользователе (имя, возраст), хранимую в атрибутах объекта класса User. Сервер должен выводить информацию о
подключенных пользователях. Клиентское приложение должно быть запущено несколько раз с различными
пользователями."""

import pickle
import socket
from homework_server_lecture10.task_2.class_User import User  # импортируем класс User

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # создаем экземпляр класса socket
sock.connect(('', 1225))  # подключаемся к серверу
#man = User('Masha', 'Serova', 14, 45, 150)
man = User('Egor', 'Petrov', 24, 70, 170)
#man = User('Ivan', 'Sergeev', 20, 58, 170)
#man = User('Irina', 'Ivanova', 34, 60, 171)
items = pickle.dumps(man)  # переводим данные в pickle формат
sock.send(items)  # отправляем сообщение серверу
sock.close()
