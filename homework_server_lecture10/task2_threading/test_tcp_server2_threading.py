import mock
from unittest.mock import MagicMock
import socket
import unittest
import sys
from homework_server_lecture10.task_2.class_User import User
sys.path.append('/home/test/python2019')
from homework_server_lecture10.task2_threading.tcp_server2_threading import TcpServer, ClientThread


class TestTcpServerWithMock(unittest.TestCase):
    def test_tcp_server(self):
        client1 = User('Masha', 'Serova', 14, 45, 150)
        client1_name = client1.__repr__()
        with mock.patch('socket.socket') as mock_socket: # создаем мок сокета, чтобы
                                                         # иметь возможность подменить
                                                         # возвращаемое значение метода recv
                                                         # https://stackoverflow.com/questions/52394540/mocking-socket-recv
            mock_socket.return_value.recv.return_value = client1_name.encode() # определяем какое значение будет
                                                                               # возвращаться методом recv
            my_server = TcpServer(host='127.0.0.1', port=5555, my_socket=mock_socket)

# class TestTcpServer(unittest.TestCase):
#     def test_tcp_server(self):
#         my_tcp = TcpServer(host='127.0.0.1', port=5555)
#         # Подменяем ClientThread(conn, addr)  mock-объектом
#         with mock.patch('ClientThread(conn, addr)') as client_thread_mock:
#             # Вместо TcpServer(host='127.0.0.1', port=5555)тоже используем mock-объект
#             client_thread = mock.Mock()
#             client_thread.name = 'MyTread'
#             client_thread.run.return_value = 0
#             cmd_mgr.set_service(service)
# self.assertTrue(service.start.called)
# cmd_mgr.run_command('status')
# # Используем assert-методы mock-объекта, которым мы подменили audit_service
# audit_service_mock.add_call.assert_called_once_with('MyService', 'run', 'status')
# service.run.assert_called_once_with('status')
# audit_service_mock.add_result.assert_called_once_with('MyService', 'run', 0)
# if __name__ == '__main__':
# unittest.main()m