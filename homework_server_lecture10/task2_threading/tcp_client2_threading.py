import socket
from homework_server_lecture10.task_2.class_User import User


class TcpClient:  # при тестах это тоже будет мок объект
    def __init__(self, host, port, client_name):
        self.host = host
        self.port = port
        self.client_name = client_name
        self._socket = None

    def run(self):
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.connect((self.host, self.port))
        self._socket.send(self.client_name.encode())
        data = self._socket.recv(1024)
        print('Received: {}'.format(data.decode()))
        self._socket.close()


if __name__ == '__main__':
    client1 = User('Masha', 'Serova', 14, 45, 150)
    client2 = User('Egor', 'Petrov', 24, 70, 170)
    client3 = User('Ivan', 'Sergeev', 20, 58, 170)
    client4 = User('Irina', 'Ivanova', 34, 60, 171)
    clients = [client1, client2, client3, client4]
    for client in clients:
        name = client.__repr__()
        my_client = TcpClient(host='127.0.0.1', port=5555, client_name=name)
        my_client.run()
