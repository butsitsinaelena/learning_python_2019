# """Напишите функцию, которая принимает строку в качестве входных данных и изменяет только гласные строки.
# Example 1:
# Input: "hello"
# Output: "holle"
#
# Example 2:
# Input: "leetcode"
# Output: "leotcede"
#
# """
# class Solution:
#     def reverseVowels(self, string: str) -> str:
#         vowels = ['a', 'A', 'e', 'E', 'o', 'O', 'i', 'I', 'u', 'U']
#         string = list(string)
#         l = []
#         for i in range(len(string)):
#             if string[i] in vowels:
#                 l.append(string[i])
#         for i in range(len(string)):
#             if string[i] in vowels:
#                 for item in reversed(l):
#                     string[i] = item
#                     l.pop()
#                     break
#         string = ''.join(string)
#         return string
#
#
# l = "race car"
# #l = "leetcode"
# l = "hello"
# d = Solution().reverseVowels(l)
# print(d)
#
#
# ###  из инета
#
# class Solution:
#     def reverseVowels(self, s: str) -> str:
#         vowels = "aeiou"
#         slist = list(s)
#         start = 0
#         end = len(slist) - 1
#
#         while(start < end):
#             if(slist[start].lower() in vowels):
#                 if(slist[end].lower() in vowels):
#                     st = slist[start]
#                     slist[start] = slist[end]
#                     slist[end] = st
#                     start = start + 1
#                     end = end - 1
#                 else:
#                     end = end - 1
#             elif (slist[end].lower() in vowels):
#                 start = start + 1
#             else:
#                 start = start + 1
#                 end = end - 1
#         return "".join(slist)
#
#
# ###  из инета


class Solution:
    def reverseVowels(self, s):
        # grab all the vowels indexes
        vowels = 'aeiouAEIOU'
        s = list(s)

        i = 0
        j = len(s) - 1

        while i < j:
            while i < j and s[i] not in vowels:
                i += 1
            while i < j and s[j] not in vowels:
                j -= 1
            s[i], s[j] = s[j], s[i]
            i += 1
            j -= 1

        return ''.join(s)


l = "hello"
l = "race car"
d = Solution().reverseVowels(l)
print(d)