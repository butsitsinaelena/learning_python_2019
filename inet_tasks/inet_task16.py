"""Получив целое число n, верните разницу между произведением его цифр и суммой его цифр.
Example 1:
Input: n = 234
Output: 15
Explanation:
Product of digits = 2 * 3 * 4 = 24
Sum of digits = 2 + 3 + 4 = 9
Result = 24 - 9 = 15

Example 2:
Input: n = 4421
Output: 21
Explanation:
Product of digits = 4 * 4 * 2 * 1 = 32
Sum of digits = 4 + 4 + 2 + 1 = 11
Result = 32 - 11 = 21
"""

class Solution:
    def subtractProductAndSum(self, number: int) -> int:
        number = str(number)
        sum = 0
        product = 1
        for item in number:
            sum += int(item)
            product *= int(item)
        return product - sum

num = 234
difference = Solution().subtractProductAndSum(num)
print(difference)


## из инета

""" functools. reduce ( функция , итерируемая [ , инициализатор ] )

    Примените функцию двух аргументов кумулятивно к элементам итерируемого слева направо, чтобы свести 
    итерируемое к одному значению. Например, reduce(lambda x, y: x+y, [1, 2, 3, 4, 5]) вычисляет ((((1+2)+3)+4)+5) . 
    Левый аргумент x - это накопленное значение, а правый аргумент y - это значение обновления из итерируемого . 
    Если присутствует необязательный инициализатор , он помещается перед элементами итерируемого в вычислении и 
    служит по умолчанию, когда итерируемый является пустым. Если инициализатор не задан и итеративный содержит 
    только один элемент, возвращается первый элемент.

    Примерно эквивалентно:

     def reduce ( function , iterable , initializer = None ):
        it = iter ( iterable )
        if initializer is None :
            value = next ( it )
        else :
            value = initializer
        for element in it :
            value = function ( value , element )
        return value

    См. itertools.accumulate() для итератора, который itertools.accumulate() все промежуточные значения. """

from functools import reduce
from operator import mul


""" operator. mul ( а , б )
operator. __mul__ ( a , b )

    Вернуть a * b , для чисел a и b . 
    https://translate.google.com/translate?hl=ru&sl=en&u=https://docs.python.org/3/library/operator.html&prev=search """

class Solution1:
    def subtractProductAndSum(self, n: int) -> int:
        digits = [int(x) for x in str(n)]
        return reduce(mul, digits) - sum(digits)


from numpy import prod

class Solution2:
    def subtractProductAndSum(self, n: int) -> int:
        return (lambda n: prod(n) - sum(n))(list(map(int,str(n))))


class Solution:
    def subtractProductAndSum(self, n: int) -> int:
        s, p = 0, 1
        for i in str(n): s += int(i); p *= int(i)
        return p - s