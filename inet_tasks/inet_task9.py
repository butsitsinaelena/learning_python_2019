"""Вам даны строки J, представляющие типы камней, которые являются драгоценными камнями, и S, представляющие камни,
которые у вас есть. Каждый символ в S - это тот тип камня, который у вас есть. Вы хотите знать, сколько камней у
вас есть и драгоценности.
Буквы в J гарантированно различны, и все символы в J и S являются буквами. Буквы чувствительны к регистру, поэтому «а»
считается камнем, отличным от «А».
Input: J = "aA", S = "aAAbbbb"
Output: 3"""


#########мое решение

class Solution:
    def numJewelsInStones(self, J: str, S: str) -> int:
        count = 0
        for s in S:
            for j in J:
                if j == s:
                    count += 1
        return count

######### из инета


class Solution1:
    def numJewelsInStones(self, J: str, S: str):
        count = 0

        for jewel in J:
            count += S.count(jewel)

        return count
########## inet else


class Solution2:
    def numJewelsInStones(self, J, S):
        return len([i for i in S if i in J])

##### inet


from collections import Counter


class Solution3(object):
    def numJewelsInStones(self, J: str, S: str) -> int:
        """
        :type J: str
        :type S: str
        :rtype: int
        """
        return sum((Counter(J * len(S)) & Counter(S)).values())

########## inet


from functools import reduce


class Solution4:
    def numJewelsInStones(self, J: str, S: str) -> int:
        return reduce(lambda x, y: x + S.count(y), J, 0)

##### самое быстрое по времени

class Solution5:
    def numJewelsInStones(self, J: str, S: str) -> int:
        matches = 0
        for j in J:
            matches += S.count(j)
        return matches

#### второе по скорости


class Solution6:
    def numJewelsInStones(self, J: str, S: str) -> int:
        count = 0
        for m_char in S:
            if (m_char in J):
                count += 1
        return count


# J = "aA"
# S = "aAAbbbb"
J = "z"
S = "ZZ"
x = Solution().numJewelsInStones(J, S)
print(x)