"""Если указан правильный IP-адрес (IPv4), верните версию этого IP-адреса с ошибками.
Измененный IP-адрес заменяет каждый период "." с "[.]".
address = "1.1.1.1"""
# мое тупое решение
# class Solution:
#     def defangIPaddr(self, address: str) -> str:
#         address = str(address)
#         address_list = []
#         while True:
#             for i in range(len(address)):
#                 if address[i] == '.':
#                     address_list.append('[.]')
#                 else:
#                     address_list.append(address[i])
#             address = ''.join(address_list)
#             return address


# решение из интернета
class Solution:
    def defangIPaddr(self, address: str) -> str:
        return address.replace(".", "[.]")

#a = "1.1.1.1"
a = "255.100.50.0"
d = Solution().defangIPaddr(a)
print(d)

# еще одно решение из инета

import re
class Solution:
    def defangIPaddr(self, address: str) -> str:
        return re.sub('\.', '[.]',address)


#a = "1.1.1.1"
a = "255.100.50.0"
d = Solution().defangIPaddr(a)
print(d)
#Note: Make sure to add a escape character (' \ ') before '.' This is because '.'
# in python regex matches any single character except newline 'n'
#TC: O(n) n=> number of characters in the string
#SC: O(1)