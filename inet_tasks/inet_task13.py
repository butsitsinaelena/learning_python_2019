"""Если задана строка и целое число k, вам нужно поменять местами первые k символов для каждых 2k символов,
считая от начала строки. Если осталось менее k символов, поменяйте их местами. Если количество символов меньше 2k,
но больше или равно k, тогда поменяйте местами первые k символов и оставьте остальные как исходные.
Ввод: s = "abcdefg", k = 2
Вывод: "bacdfeg"
Ограничения:
Строка состоит только из нижних английских букв.
Длина заданной строки и k будет в диапазоне [1, 10000]"""


string = "abcdefg"
a = string[0:2][::1]
print(a)

class Solution:
    def reverseStr(self, string: str, number: int):
        if number > len(string):
            return string[::-1]
        lst = []
        start = 0
        end = start + number
        count_numb = len(string)
        count = 2
        t = -1
        while True:
            if count % 2 == 0:
                t = -1
            else:
                t = 1
            if number <= count_numb:
                a = string[start:end][::t]
                lst.extend(a)
                count_numb -= number
                start += number
                end += number
                count += 1
            else:
                if count % 2 == 0:
                    a = string[start:end][::-1]
                else:
                    a = string[start:end]
                lst.extend(a)
                new_string = ''.join(lst)
                return new_string


s = "abcdefg"
k = 6
d = Solution().reverseStr(s, k)
print('d = ', d)

#### из инета

class Solution:
    def reverseStr(self, s: str, k: int) -> str:
        start = 0
        rst=""
        count=0
        while(start<len(s)):
            count+=1
            if(count%2!=0):
                temp=s[start:start+k]
                rst=rst+temp[::-1]
                start+=k
            else:
                rst=rst+s[start:start+k]
                start+=k
        return rst

###### из инета

class Solution1:
    def reverseStr(self, s: str, k: int) -> str:
        new_s = ''
        i = 0
        while len(s) >= 2*k*i:
            new_s += s[2*k*i:2*k*(i+1) - k][::-1] + s[2*k*(i+1) - k:2*k*(i+1)]
            i += 1
        return new_s


###### из инета

class Solution2:
    def reverseStr(self, s: str, k: int) -> str:
        res = ''
        for i in range(len(s) // (2 * k)):
            res += s[2 * k * i:2 * k * i + k][::-1] + s[2 * k * i + k:2 * k * (i + 1)]

        remainder = len(s) % (2 * k)

        if remainder <= k and remainder > 0:
            res += s[-remainder:][::-1]
        elif remainder > k:
            res += s[-remainder:-remainder + k][::-1] + s[-remainder + k:]

        return res