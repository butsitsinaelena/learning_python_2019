"""Римские цифры представлены семью различными символами: I, V, X, L, C, D и M.
Например, два пишется как II в римской цифре, только два сложены вместе. Двенадцать написано как, XII,
что просто X + II. Число двадцать семь написано как XXVII, что является XX + V + II.
Римские цифры обычно пишутся слева направо от наименьшего к наименьшему. Тем не менее, цифра для четырех не IIII.
Вместо этого число четыре написано как IV. Потому что один перед пятью мы вычитаем, получая четыре. Тот же принцип
применим к числу девять, которое написано как IX. В шести случаях используется вычитание:

Я могу поставить перед V (5) и X (10), чтобы сделать 4 и 9.
X можно поставить перед L (50) и C (100), чтобы составить 40 и 90.
C можно поставить перед D (500) и M (1000) до 400 и 900.

Получив римскую цифру, преобразуйте ее в целое число. Вход гарантированно находится в диапазоне от 1 до 3999.

MCMXCIV = 1994 """


class Solution:
    def romanToInt(self, number):
        int_number = 0
        for i in range(len(number)):
            if number[i] == 'M':
                int_number += 1000
            elif number[i] == 'D':
                int_number += 500
            elif number[i] == 'C':
                if i == len(number)-1:
                    int_number += 100
                else:
                    if number[i+1] != 'M' and number[i+1] != 'D':
                        int_number += 100
                    else:
                        int_number -= 100
            elif number[i] == 'L':
                int_number += 50
            elif number[i] == 'X':
                if i == len(number)-1:
                    int_number += 10
                else:
                    if number[i+1] != 'L' and number[i+1] != 'C':
                        int_number += 10
                    else:
                        int_number -= 10
            elif number[i] == 'V':
                int_number += 5
            elif number[i] == 'I':
                if i == len(number)-1:
                    int_number += 1
                else:
                    if number[i+1] != 'V' and number[i+1] != 'X':
                        int_number += 1
                    else:
                        int_number -= 1
        return int_number


n = Solution().romanToInt('CMIV')
print('n = ', n)

