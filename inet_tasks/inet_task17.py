"""Реализуйте функцию ToLowerCase (), которая имеет строковый параметр str и возвращает ту же строку в нижнем
 регистре.

Example 1:
Input: "Hello"
Output: "hello"

Example 2:
Input: "here"
Output: "here"

Example 3:
Input: "LOVELY"
Output: "lovely"

"""

class Solution:
    def toLowerCase(self, string: str) -> str:
        return string.lower()


l = "Hello"
l = "LOVELY"
d = Solution().toLowerCase(l)
print(d)

# из интернена

class Solution:
    def toLowerCase(self, str: str) -> str:
        ans = ""
        for item in str:
            number = ord(item)  # Возвращает числовое представление для указанного символа.  ord(chr) -> int
            # ord('a')  # 97  ord('\u2020')  # 8224
            if number <= 90 and number >= 65:
                number += 32
                item = chr(number)  # Возвращает символ по его числовому представлению.  chr(i)-> str  chr(97)  # a
            ans += item

        return ans