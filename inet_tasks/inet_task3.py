# """Определите, является ли целое число палиндромом.
# Целое число - это палиндром, когда оно читает то же самое, что и вперед.
# Вы решаете это без преобразования целого числа в строку?"""
#
# print('\n______my code______\n')
# class Solution:
#     def isPalindrome(self, x):
#         str_x = str(x)
#         polinom_x = str_x[::-1]
#         if str_x == polinom_x:
#             return True
#         else:
#             return False
#
#
# f = Solution().isPalindrome(-3451543)
# print(f)
#
# print('\n______like code______\n')
#
# class Solution:
#     def isPalindrome(self, x):
#         """
#         :type x: int
#         :rtype: bool
#         """
#         return str(x) == str(x)[::-1]
#
#
# f = Solution().isPalindrome(3451543)
# print(f)
#
#
# print('\n______like code______\n')


class Solution:
    def isPalindrome(self, x: int) -> bool:
        if x < 0: return False
        if x == 0: return True

        import math
        j = int(math.log10(x))  # first digit power
        if j == 0: return True  # one digit number

        i = 0  # last digit power

        while i < j:
            if (x // (10 ** i)) % 10 is not (x // (10 ** j)) % 10: return False
            i += 1
            j -= 1

        return True


f = Solution().isPalindrome(3451543)
print(f)