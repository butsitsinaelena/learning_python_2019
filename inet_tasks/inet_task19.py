"""Допустимая строка в скобках: пустая (""), "(" + A + ")" или A + B, где A и B - допустимые строки в скобках,
а + - конкатенация строк. Например, "", "()", "(()) ()" и "(() (()))" - все допустимые строки скобок.
Допустимая строка скобок S является примитивной, если она не пуста, и не существует способа разбить ее на S = A + B,
с непустыми допустимыми строками скобок A и B.
При наличии допустимой строки скобок S рассмотрим ее примитивную декомпозицию: S = P_1 + P_2 + ... + P_k,
где P_i - строки примитивных допустимых скобок.
Вернуть S после удаления крайних скобок каждой примитивной строки в примитивной декомпозиции S.

Example 1:
Input: "(()())(())"
Output: "()()()"
Explanation:
The input string is "(()())(())", with primitive decomposition "(()())" + "(())".
After removing outer parentheses of each part, this is "()()" + "()" = "()()()".

Example 2:
Input: "(()())(())(()(()))"
Output: "()()()()(())"
Explanation:
The input string is "(()())(())(()(()))", with primitive decomposition "(()())" + "(())" + "(()(()))".
After removing outer parentheses of each part, this is "()()" + "()" + "()(())" = "()()()()(())".

Example 3:
Input: "()()"
Output: ""
Explanation:
The input string is "()()", with primitive decomposition "()" + "()".
After removing outer parentheses of each part, this is "" + "" = "".
"""


class Solution:
    def removeOuterParentheses(self, string):
        brackets_list = []
        count = 0
        index = 0
        brackets = ""
        for i in range(len(string)):
            if string[i] == '(':
                count += 1
            elif string[i] == ')':
                count -= 1
            if count == 0:
                bracket = string[index+1:i]
                index = i+1
                #brackets_list.append(bracket)
                brackets += bracket
        #return ''.join(brackets_list)
        return brackets


l = "()()"
#l = "(()())(())(()(()))" # "()()()()(())" "(()())+(())+(()+(()))"

d = Solution().removeOuterParentheses(l)
print('d = ', d)

# из инета

class Solution1:
    def removeOuterParentheses(self, S: str) -> str:
        s = []
        rm = set()
        for i,c in enumerate(S):
            if c == '(':
                s.append(i)
            else:
                j = s.pop()
                if not s:
                    rm.add(i)
                    rm.add(j)
        return ''.join([c for i,c in enumerate(S) if i not in rm])


# из инета


class Solution2:
    def removeOuterParentheses(self, S: str) -> str:
        return ''.join([prim[1:-1] for prim in self.primitives(S)])

    def primitives(self, S):
        tot, start, out = 0, 0, []
        for i in range(len(S)):
            if S[i] == '(':
                tot += 1
            else:
                tot -= 1
            if tot == 0:
                out.append(S[start:i + 1])
                start = i + 1
        return out

# из инета

class Solution3:
    def removeOuterParentheses(self, S: str) -> str:
        num_encaps = 0
        prim_list = []
        for index in range(0,len(S)):
            if S[index] == '(':
                if num_encaps == 0:
                    prim_list.append(index)
                num_encaps+=1
            else:
                num_encaps-=1
                if (num_encaps ==0):
                    prim_list.append(index)
        final_string = ""
        print(prim_list)
        #print(S[1:5])
        #print(S[7:9])
        for first_index in range(0,len(prim_list), 2):
            final_string = final_string + S[prim_list[first_index]+1:prim_list[first_index +1]]
            #print(prim_list[first_index], prim_list[first_index +1])
            #print(final_string)
        return final_string

# из инета
class Solution4:
    def removeOuterParentheses(self, S: str) -> str:
        res, level, last = "", 0, 0
        for i in range(len(S)):
            level += 1 if S[i] == '(' else -1
            if level == 0:
                res += S[last+1:i]
                last = i+1
        return res


# из инета
class Solution5:
    def removeOuterParentheses(self, S: str) -> str:
        res = ""
        tmp = ""
        flag = 0
        for i in S:
            tmp+=i
            if i == "(":
                flag+=1
            else:
                flag-=1
            if flag == 0:
                res+=tmp[1:-1]
                tmp=""
        return res

# def stringToString(input):
#     import json
#
#     return json.loads(input)
#
#
# def main():
#     import sys
#     import io
#     def readlines():
#         for line in io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8'):
#             yield line.strip('\n')
#
#     lines = readlines()
#     while True:
#         try:
#             line = next(lines)
#             S = stringToString(line);
#
#             ret = Solution().removeOuterParentheses(S)
#
#             out = (ret);
#             print(out)
#         except StopIteration:
#             break
#
#
# if __name__ == '__main__':
#     main()