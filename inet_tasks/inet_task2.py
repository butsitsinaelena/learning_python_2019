"""Учитывая 32-разрядное целое число со знаком, обратные цифры целого числа.
Предположим, мы имеем дело со средой, которая может хранить целые числа только в диапазоне 32-разрядных целых чисел
со знаком: [−231, 231 - 1]. Для решения этой проблемы предположим, что ваша функция возвращает 0, когда переполненное
целое число переполняется. -(2**32 - 1) = −2147483648
(2**32 - 1) = 2147483647 """


class Solution:
    def reverse(self, x):
        if -(2**32 - 1) <= x <= (2**32 - 1):
            if x < 0:
                x = abs(x)
                xi = -1
            else:
                xi = 1
            list_x = list(str(x))
            list_x.reverse()
            new_x = ''.join(list_x)
            x = int(new_x) * xi
            return x
        else:
            return 0


f = Solution().reverse(21474836412)
print(f, type(f))


##########################################################################

# Runtime: 40 ms, faster than 99.95% of Python3 online submissions for Reverse Integer.
# Memory Usage: 13.2 MB, less than 5.71% of Python3 online submissions for Reverse Integer.

class Solution:
    def reverse(self, x: int) -> int:
        if x > 0:  # handle positive numbers
            a =  int(str(x)[::-1])
        if x <=0:  # handle negative numbers
            a = -1 * int(str(x*-1)[::-1])
        # handle 32 bit overflow
        mina = -2**31
        maxa = 2**31 - 1
        if a not in range(mina, maxa):
            return 0
        else:
            return a

