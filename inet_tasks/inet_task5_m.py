"""Римские цифры представлены семью различными символами: I, V, X, L, C, D и M.
Например, два пишется как II в римской цифре, только два сложены вместе. Двенадцать написано как, XII, что
просто X + II. Число двадцать семь написано как XXVII, что является XX + V + II.
Римские цифры обычно пишутся слева направо от наименьшего к наименьшему. Тем не менее, цифра для четырех не IIII.
Вместо этого число четыре написано как IV. Потому что один перед пятью мы вычитаем, получая четыре.
Тот же принцип применим к числу девять, которое написано как IX. В шести случаях используется вычитание:

    I могу поставить перед V (5) и X (10), чтобы сделать 4 и 9.
    X можно поставить перед L (50) и C (100), чтобы составить 40 и 90.
    C можно поставить перед D (500) и M (1000) до 400 и 900.

Дано целое число, преобразовать его в римскую цифру. Вход гарантированно находится в диапазоне от 1 до 3999."""


class Solution:
    def intToRoman(self, number):
        number = list(str(number))
        len_number = len(number)
        int_number = []
        count = len_number
        for i in range(len_number):
            number[i] = int(number[i])
            if count == 1:
                num1 = 'I'
                num2 = 'V'
                num3 = 'X'
            elif count == 2:
                num1 = 'X'
                num2 = 'L'
                num3 = 'C'
            elif count == 3:
                num1 = 'C'
                num2 = 'D'
                num3 = 'M'
            else:
                num1 = 'M'
            if 1 <= number[i] <= 3:
                x = num1 * number[i]
                int_number.insert(i, x)
            elif number[i] == 4:
                x = num1 + num2
                int_number.insert(i, x)
            elif number[i] == 5:
                x = num2
                int_number.insert(i, x)
            elif 6 <= number[i] <= 8:
                x = num2 + num1 * (number[i] - 5)
                int_number.insert(i, x)
            elif number[i] == 9:
                x = num1 + num3
                int_number.insert(i, x)
            count -= 1
        int_number1 = ''.join(int_number)
        return int_number1


n = Solution().intToRoman(101)
print('n = ', n)

##########################################################################
"""Loop through the
digits(low to high) while maintaining the magnitude of which they belong.Take the unit as an example,

if digit is 0, 1, 2, 3, then roman is "I" * digit;
if digit is 4, 5, 6, 7, 8, then roman is "I" * max(0, 5-digit) + "V" + "I" * max(0, digit-5);
if digit is 9, then roman is IX."""


class Solution2:
    def intToRoman(self, num: int) -> str:
        symbols = {1: "I", 5: "V", 10: "X", 50: "L", 100: "C", 500: "D", 1000: "M"}

        ans = []
        m = 1  # magnitude
        while num:
            num, digit = divmod(num, 10)
            if digit < 4:
                ans.append(digit * symbols[m])
            elif digit < 9:
                ans.append(max(5 - digit, 0) * symbols[m] + symbols[5 * m] + max(digit - 5, 0) * symbols[m])
            else:
                ans.append(symbols[m] + symbols[10 * m])
            m *= 10
        return "".join(reversed(ans))

##################################################################################
import itertools


class Solution3:
    def intToRoman(self, num: int) -> str:
        symmap = ((),(0,),(0,0),(0,0,0),(1,0),(1,),(0,1),(0,0,1),(0,0,0,1),(2,0))
        symbol = 'IVXLCDM'
        result = ''
        for i in itertools.count(0, 2):
            for j in symmap[num % 10]:
                result += symbol[i + j]
            num //= 10
            if not num:
                break
        return result[::-1]

############################################

    def intToRoman(self, num: int) -> str:
        return_str = ""
        dictt = {1000: "M",
                 900: "CM",
                 500: "D",
                 400: "CD",
                 100: "C",
                 90: "XC",
                 50: "L",
                 40: "XL",
                 10: "X",
                 9: "IX",
                 5: "V",
                 4: "IV",
                 1: "I"
                 }
        while num > 0:
            for i in dictt:
                if num - i >= 0:
                    return_str += dictt[i]
                    num = num - i
                    break
        return return_str

