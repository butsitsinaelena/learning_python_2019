"""Для данной строки вам необходимо изменить порядок символов в каждом слове в предложении,
сохраняя при этом пробелы и начальный порядок слов.
Input: "Let's take LeetCode contest"
Output: "s'teL ekat edoCteeL tsetnoc" """


class Solution:
    def reverseWords(self, string):
        list_string = string.split(' ')
        l_string = []
        for item in list_string:
            item = list(item)
            item.reverse()
            st = ''.join(item)
            l_string.append(st)
        new_string = ' '.join(l_string)
        return new_string


a = "Let's take LeetCode contest"
d = Solution().reverseWords(a)
print('d = ', d)

#### из инета

class Solution1:
    def reverseWords(self, s: str) -> str:
        strs = s.split(" ")
        newstr = ""

        for c in strs:
            newstr += c[::-1] + " "

        return newstr[:-1]

#####  из инета


class Solution2:
    def reverseWords(self, str: str) -> str:
        wordrev=[w[::-1] for w in str.split(" ")]
        return " ".join(wordrev)

#### из инета


class Solution3:
    def reverseWords(self, s: str) -> str:
        return ' '.join([x[::-1] for x in s.split(' ')])

####### из инета


class Solution4:
    def reverseWords(self, s: str) -> str:
        temp = s.split()
        print(temp)
        for i in range(len(temp)):
            temp[i] = temp[i][::-1]
            print(temp[i])
        return " ".join(temp)

a = "Let's take LeetCode contest"
d = Solution4().reverseWords(a)
print('d = ', d)