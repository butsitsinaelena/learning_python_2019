""" 385 Имея вложенный список целых чисел, представленных в виде строки, реализуйте анализатор для его десериализации.
Каждый элемент является либо целым числом, либо списком, элементы которого также могут быть целыми числами или
другими списками. Примечание: вы можете предположить, что строка правильно сформирована:
     Строка не пустая.
     Строка не содержит пробелов.
     Строка содержит только цифры 0-9, [, - ,,]."""

# Это интерфейс, который позволяет создавать вложенные списки.
# Вы не должны реализовывать это или рассуждать о его реализации

# class NestedInteger:
#    def __init__(self, value=None):
#        """
#        If value is not specified, initializes an empty list.
#        Otherwise initializes a single integer equal to value.
#        """
#
#    def isInteger(self):
#        """
#        @return True if this NestedInteger holds a single integer, rather than a nested list.
#        :rtype bool
#        """
#
#    def add(self, elem):
#        """
#        Set this NestedInteger to hold a nested list and adds a nested integer elem to it.
#        :rtype void
#        """
#
#    def setInteger(self, value):
#        """
#        Set this NestedInteger to hold a single integer equal to value.
#        :rtype void
#        """
#
#    def getInteger(self):
#        """
#        @return the single integer that this NestedInteger holds, if it holds a single integer
#        Return None if this NestedInteger holds a nested list
#        :rtype int
#        """
#
#    def getList(self):
#        """
#        @return the nested list that this NestedInteger holds, if it holds a nested list
#        Return None if this NestedInteger holds a single integer
#        :rtype List[NestedInteger]
#        """

