"""Напишите функцию, чтобы найти самую длинную строку общего префикса среди массива строк.
Если общего префикса нет, верните пустую строку
["flower","flow","flight"]"""


# class Solution:
#     def longestCommonPrefix(self, list_str):
#         if len(list_str) == 1:
#             return list_str[0]
#         if len(list_str) == 0:
#             return ''
#         count = 0
#         prefix = ""
#         for i in range(len(list_str)):
#             while True:
#                 pref_count = 0
#                 t = len(list_str[i]) - count
#                 if t < 0:
#                     return ''
#                 if list_str[i] == '':
#                     return ''
#                 for j in range(i+1, len(list_str)):
#                     if list_str[i][:t] in list_str[j][:t]:
#                         prefix = list_str[i][:t]
#                         pref_count += 1
#                         print('pr = ', prefix)
#                     else:
#                         prefix = 0
#                 if prefix != 0 and pref_count == len(list_str)-1:
#                     return prefix
#                 else:
#                     count += 1


#l = ["flower", "flow", "flight"]
#l = ["dog", "racecar", "car"]
#l = ["caa", "", "a", "acb"]
#l = ["aa","a"]
#l = ["aca","cba"]
#l = ["b", "cb", "cab"]
#l = []
#l = ["aaa", "aa", "aaa"]
# d = Solution().longestCommonPrefix(l)
# print(d)



class Solution:
    def longestCommonPrefix(self, list_str):
        pref = ''
        if len(list_str) == 1:
            return list_str[0]
        if len(list_str) == 0:
            return ''
        for i in range(len(list_str)):
            if list_str[i] == '' or list_str[0][0] != list_str[i][0]:
                return ""
        j = 0
        for item in list_str[0]:
            count = 0
            for i in range(1, len(list_str)):
                if item == list_str[i][j]:
                    count += 1
                else:
                    break
            if count == len(list_str) - 1:
                j += 1
                pref += item
            else:
                break
        return pref


#l = ["flower", "flow", "flight"]
#l = ["dog", "racecar", "car"]
#l = ["caa", "", "a", "acb"]
l = ["aa","a"]
#l = ["aca","cba"]
#l = ["b", "cb", "cab"]
#l = []
#l = ["aaa", "aa", "aaa"]
#l = ["abab","aba",""]
#l = ["c","acc","ccc"]
#l = ["ac","aacb"]

d = Solution().longestCommonPrefix(l)
print(d)
l = sorted(l)
print('l sort = ', l)

### самая быстрая из инета

# class Solution2:
#     def longestCommonPrefix(self, strs):
#         """
#         :type strs: List[str]
#         :rtype: str
#         """
#         if len(strs) == 0:
#             return ''
#         res = ''
#         #strs = sorted(strs)
#         for i in strs[0]:
#             print('st = ', strs[0])
#             print('i = ', i)
#             if strs[-1].startswith(res+i):
#                 res += i
#             else:
#                 break
#         return res
#
# d = Solution2().longestCommonPrefix(l)
# print(d)


