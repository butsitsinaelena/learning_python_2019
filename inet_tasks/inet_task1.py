"""Получив массив целых чисел, верните индексы двух чисел так, чтобы они суммировались с определенной целью.
Вы можете предположить, что каждый вход будет иметь только одно решение, и вы не можете использовать
один и тот же элемент дважды."""
#
# print('\n__________twoSum___________\n')
#
#
# def twoSum(my_list, num):
#     for i in range(len(my_list)):
#         for j in range(i+1, len(my_list)):
#             if my_list[i] + my_list[j] == num:
#                 return [i, j]
#     else:
#         return 'sum two elements in the list {} != {}'.format(my_list, num)
#
#
# num_list = [8, 7, 1, 13]
# target = 9
# f = twoSum(num_list, target)
# print(f)
#
# print('\n__________twoSum with class1___________\n')
#
#
class Solution:
    def __init__(self, lst, num):
        self.lst = lst
        self.num = num

    def twoSum(self):
        for i in range(len(self.lst)):
            for j in range(i + 1, len(self.lst)):
                if self.lst[i] + self.lst[j] == self.num:
                    return [i, j]
        else:
            return 'sum two elements in the list {} != {}'.format(self.lst, self.num)


lst = [3, 2, 4]
num = 6

f1 = Solution(lst, num)
print(f1.twoSum())


# print('\n__________twoSum with class2___________\n')
#
#
# class Solution:
#     def twoSum(self, lst, num):
#         for i, ls in enumerate(lst):
#             number = num - ls
#             if number in lst:
#                 return [i, lst.index(number)]
#             else:
#                 return []
#
#
# lst = [3, 2, 4]
# num = 6
#
# f1 = Solution().twoSum(lst, num)
# print(f1)