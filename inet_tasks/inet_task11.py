"""Сбалансированные строки - это те, которые имеют одинаковое количество символов «L» и «R».
С учетом сбалансированной строки s разбейте ее на максимальное количество сбалансированных строк.
Вернуть максимальное количество разбитых сбалансированных строк.
Input: s = "RLRRLLRLRL"
Output: 4"""


class Solution:
    def balancedStringSplit(self, string):
        balance = 0
        count_balance = 0
        let1 = string[0]
        for i in range(len(string)):
            if string[i] == let1:
                count_balance += 1
            else:
                count_balance -= 1
            if count_balance == 0:
                balance += 1
        return balance


l = "RLRRLLRLRL"
#l = "LLLLRRRR"
#l = "RLLLLRRRLR"
#l = "RLRRRLLRLL"

d = Solution().balancedStringSplit(l)
print(d)


####  из инета со словарями

class Solution1:
    def balancedStringSplit(self, s: str) -> int:
        r = 0
        c = {"L" : 0, "R": 0}
        for l in s:
            c[l] += 1
            if c["L"] == c["R"]:
                r += 1
        return r


#### из инета

class Solution2:
    def balancedStringSplit(self, s: str) -> int:
        l_count = 0
        r_count = 0
        total = 0
        for letter in s:
            if letter == 'R':
                r_count += 1
            if letter == 'L':
                l_count += 1
            if (l_count == r_count):
                total += 1
                l_count = 0
                r_count = 0

        return total

#####  из инета

class Solution3:
    def balancedStringSplit(self, s: str) -> int:
        one_stack = []
        count = 0
        for item in s:
            if not one_stack:
                one_stack.append(item)
            else:
                if one_stack[-1] == item:
                    one_stack.append(item)
                else:
                    one_stack.pop()
                    if len(one_stack) == 0:
                       count += 1

        return count
