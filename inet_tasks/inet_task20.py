"""Разработайте стек, который поддерживает push, pop, top и извлечение минимального элемента за постоянное время.
     push (x) - Вставить элемент x в стек.
     pop () - удаляет элемент сверху стека.
     top () - Получить верхний элемент.
     getMin () - Получить минимальный элемент в стеке.

MinStack minStack = new MinStack();
minStack.push(-2);
minStack.push(0);
minStack.push(-3);
minStack.getMin();   --> Returns -3.
minStack.pop();
minStack.top();      --> Returns 0.
minStack.getMin();   --> Returns -2."""


class MinStack:
    def __init__(self):
        self.stack_list = []

    def push(self, element: int) -> None:
        self.stack_list.append(element)
        print(self.stack_list)

    def pop(self) -> None:
        self.stack_list.pop()
        print(self.stack_list)

    def top(self) -> int:
        return self.stack_list[-1]

    def getMin(self) -> int:
        return min(self.stack_list)


minStack = MinStack()
minStack.push(-2)
minStack.push(0)
minStack.push(-3)

print(minStack.getMin())
minStack.pop()
print(minStack.top())
print(minStack.getMin())


####################

# -----Using a list as a stack-----#
# -----O(n) time and O(k)  space for second stack - when k is number of times an integer pushed into the stack is a minimum-----#

class MinStack:
    def __init__(self):
        self.stack = []
        self.minVal = []

    def push(self, x: int) -> None:
        self.stack.append(x)
        if self.minVal:
            val, count = self.minVal[-1]
            if val > x:
                self.minVal.append((x, 1))
            elif val == x:
                self.minVal[-1] = (val, count + 1)
        else:
            self.minVal.append((x, 1))

    def pop(self) -> None:
        val, count = self.minVal[-1]
        if val == self.stack[-1]:
            count -= 1
            if count == 0:
                self.minVal.pop()
            else:
                self.minVal[-1] = (val, count)
        return self.stack.pop()

    def top(self) -> int:
        return self.stack[-1]

    def getMin(self) -> int:
        val, count = self.minVal[-1]
        return val