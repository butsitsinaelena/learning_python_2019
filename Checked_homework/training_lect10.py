"""Написать приложение, которое непрерывно выводит на экран введенный
пользователем символ. Пользователь может задать новый символ, не
останавливая вывод старого символа. При это программа должна
мгновенно переключиться на вывод нового символа. Пока ни один символ
не введен – программа ничего не выводит, как только введен символ ‘q’ –
программа завершается."""

import threading
import time


print('________with event_________________')


def input_symbol():
    global some_symbol
    event.wait()
    while True:
        symbol = input()
        some_symbol = symbol
        if symbol == 'q':
            break


some_symbol = input('Enter some_symbol = ')
if some_symbol == 'q':
    exit(0)
event = threading.Event()
thr = threading.Thread(target=input_symbol)
thr.start()

event.set()
while True:
    if some_symbol != 'q':
        print(some_symbol)
        time.sleep(2)
    else:
        break

thr.join()


print('________with mutex___________')


def input_symbol():
    global some_symbol
    with mutex:
        while True:
            symbol = input()
            some_symbol = symbol
            if symbol == 'q':
                break


some_symbol = input('Enter some_symbol = ')
if some_symbol == 'q':
    exit(0)
mutex = threading.RLock()
thr = threading.Thread(target=input_symbol)
thr.start()

while True:
    if some_symbol != 'q':
        print(some_symbol)
        time.sleep(2)
    else:
        break

thr.join()






