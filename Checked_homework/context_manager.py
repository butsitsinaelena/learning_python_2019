"""Напишите свой менеджер контекста, замеряющий и показывающий время исполнения кода внутри него. """
""" Какие обьекты могут поддерживать интерфейс итератора?"""

from datetime import datetime

class MyContextManager:
    def __enter__(self):
        self.start = datetime.now()
        print('Start of MyContextManager work = ', self.start)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.end = datetime.now() - self.start
        print('Code execution time = ', self.end)
        print('Code completed. Error info: type - {}'.format(exc_type))


with MyContextManager():
    def lazy_range():
        for i in range(10000):
            yield i
    l = lazy_range()
    I = []
    for i in l:
        I.append(i)
    print(I)




