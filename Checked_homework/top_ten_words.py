""" Написать программу для вывода на экран топ-10 чаще всего встречающихся слов длиннее 5 букв в тексте
(используя Counter). В качестве текста использовать любую общедоступную книгу."""

from datetime import datetime
import collections
import itertools

print('\n_______побуквенно обходить текст ____________________________________\n')
start = datetime.now()
with open('test_text.txt', 'r') as file:
    text = file.read()

list_letter = []
for letter in text:
    if letter not in ('!', '?', ',', ':', '.', '-', ';', '"'):
        list_letter.append(letter)

text_without_punctuation = ''.join(list_letter)
words_list = text_without_punctuation.split()
words_more_than_5_letters = list(itertools.filterfalse(lambda x: len(x) < 6, words_list))
counter_words = collections.Counter(words_more_than_5_letters)
top_ten_words = counter_words.most_common(10)
print('top_ten = ', top_ten_words)
print('1. Total calculation time in seconds: {}'.format(datetime.now() - start))

print('\n_______поиск знаков препинания и их удаление с помощью replace_________\n')
start = datetime.now()
with open('test_text.txt', 'r') as file:
    text = file.read()

for i in ('!', '?', ',', ':', '.', '-', ';', '"'):
    text = text.replace(i, '')

text_without_punctuation = text
words_list = text_without_punctuation.split()
words_more_than_5_letters = list(itertools.filterfalse(lambda x: len(x) < 6, words_list))
counter_words = collections.Counter(words_more_than_5_letters)
top_ten_words = counter_words.most_common(10)
print('top_ten = ', top_ten_words)
print('2. Total calculation time in seconds: {}'.format(datetime.now() - start))