import itertools

"""Часто задача программиста заключается в том, чтоб найти в документации готовую функцию, которая реализует 
необходимое решение. Данное задании предполагает самостоятельное изучение документации к библиотеке itertools
(это набор готовых итераторов), чтобы подобрать те функции, которые дадут правильные ответы на следующие вопросы 
(иногда надо будет добавить свои аргументы при вызове функций помимо тех, что указаны в задании):

• Функция должна принимать три массива ([1, 2, 3], [4, 5], [6, 7]), а вернуть
лишь один массив ([1, 2, 3, 4, 5, 6, 7])
• Функция принимает массив (['hello', 'i', 'write', 'cool', 'code']) и возвращает
массив из элементов, у которых длина не меньше пяти (['hello', 'write',
'world'])
• Функция выдает на строку 'password' все возможные комбинации вида
([('p', 'a', 's', 's'), ('p', 'a', 's', 'w'), ('p', 'a', 's', 'o'), ...)

Требуется написать код, который использует указанные входные данные и выводит на экран возвращаемое значение.
Помните, что функции могут возвращать генератор, который нужно "развернуть" для вывода на экран."""


def sum_list():
    my_list = list(itertools.chain(list1, list2, list3))
    return my_list


def sum_list1():  # еще один вариант
    my_list1 = list(itertools.chain.from_iterable(itertools.chain([list1, list2, list3])))
    return my_list1


def five_letters():  # без использования itertools
    words_of_five_letters_or_more = []
    for i in range(len(list_of_words)):
        if len(list_of_words[i]) >= 5:
            words_of_five_letters_or_more.append(list_of_words[i])
    return words_of_five_letters_or_more


def five_letters_with_itertools():  # c использованием itertools
    words_of_five_letters_or_more = list(itertools.filterfalse(lambda x: len(x) < 5, list_of_words))
    return words_of_five_letters_or_more


def random_combinations(my_str):
    my_combination = list(itertools.combinations(my_str, 4))
    return my_combination


def random_combinations1(my_str):  # другой вариант с повторяющимися символами
    my_combination1 = list(itertools.combinations_with_replacement(my_str, 4))
    return my_combination1


print('\n------------------задание 1---------------------')
list1 = [1, 2, 3]
list2 = [4, 5]
list3 = [6, 7]
print('\n----вариант 1')
print('joint list = ', sum_list())
print('\n----вариант 2')
print('joint list2 = ', sum_list1())

print('\n------------------задание 2---------------------')
list_of_words = ['hello', 'i', 'write', 'cool', 'code', 'world']

print('\n---- моя функция ')
new_mass_my = five_letters()
print('new_mass_my = ', new_mass_my)

print('\n---- with itertools')
new_mass_iter = five_letters_with_itertools()
print('new_mass_iter= ', new_mass_iter)


print('\n------------------задание 3---------------------')
print(random_combinations('password'))
print(random_combinations1('password'))
