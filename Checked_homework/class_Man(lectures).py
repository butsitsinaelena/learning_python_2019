"""В отдельном файле:
1.  Написать класс Man, который будет содержать информацию о человеке:
имя, рост, вес, год рождения.
2.  Определить конструктор, создать 4 объекта класса Man. Вывести на экран все объекты.
3.  Переопределить поведение функции str для объектов чтобы выводилось имя и рост.
4.  Попробовать сравнить два объекта между собой (больше, меньше, равно?)
5.  Определить метод сравнения двух людей по росту:
(man1 < man2) ->  должно возвращать True либо False.
6.  Написать свой класс исключений HeightError и выводить ошибку, если при создании объекта рост - не число.
7.  Написать метод, возвращающий возраст get_age()

8.  Добавить свойство age, которое бы вычислялось на основе возраста, но переопределить его было нельзя:
man1.age  ->  возвращает возраст
man1.age = 17  ->  ошибка

9. @property - вы можете написать setter для свойства age, чтобы например когда пишут man.age = 32 в соответствии
с этим менялся только год рождения. Почитайте про getter'ы и setter'ы в питоне. Getter у вас уже написан (то, что
требовалось, это просто дополнительно, если вам интересно).
- лучше продумать ввод объектов класса"""


import datetime


class Man:
    def __init__(self, name, height, weight, year_of_birth, month_birth, day_birth):
        if not isinstance(height, int) and not isinstance(height, float):
            raise HeightError()
        self.name = name
        self.height = height
        self.weight = weight
        self.year_of_birth = year_of_birth
        self.day_birth = day_birth
        self.month_birth = month_birth

    def __str__(self):
        return 'My name is' + ' ' + self.name + ', ' + 'my height is' + ' ' + str(self.height) + ' ' + 'sm'

    def __lt__(self, other):
        return self.height < other.height

    def __get_age(self):
        data_today = datetime.date.today()
        months_left = self.month_birth - data_today.month
        day_left = self.day_birth - data_today.day
        if months_left == 0:
            if day_left < 0:
                age = int(data_today.year) - self.year_of_birth
                return age
            else:
                age = int(data_today.year) - self.year_of_birth -1
                return age
        if months_left < 0:
            age = int(data_today.year) - self.year_of_birth
        else:
            age = int(data_today.year) - self.year_of_birth -1
        return age

    @property
    def age(self):
        return self.__get_age()

    @age.setter
    def age(self, value):
        self.__get_age()
        data_today = datetime.date.today()
        if self.__get_age() != value:
            self.year_of_birth = int(data_today.year) - value


class HeightError(TypeError):
    pass


def check_man(name, height, weight, year_of_birth, month_birth, day_birth):
    try:
        man = Man(name, height, weight, year_of_birth, month_birth, day_birth)
        return man
    except HeightError:
        print("Height is not a digit")


man1 = check_man('Jakob', 180.5, 80, 2000, 10, 13)
man2 = check_man('Alex', 175, 90, 1989, 3, 31)
man3 = check_man('Filip', 128, 25, 2015, 6, 14)
man4 = check_man('Martin', '190', 95, 1988, 10, 23)
man5 = check_man('Ignat', 160, 75, 1985, 1, 23)

list_man = [man1, man2, man3, man4, man5]
correct_list_man = []
print('_______________ Создано 4 объекта класса_______________________________')    
for i, list_man in enumerate(list_man, start=1):
    if list_man is not None:
        print('{} Name  = {} '.format(i, list_man.name))
        print('Height =', list_man.height)
        print('Weight = ', list_man.weight)
        print('Year of birth = ', list_man.year_of_birth)
        print('age = ', list_man.age)
        print('________________________')
        correct_list_man.append(list_man)
    else:
        print('The {} item in the list_man is {} - height is not correct'.format(i, list_man))
        print('________________________')

print(correct_list_man)

print('______________Переопределен метод __str____________________________')
print(man1)
print(man2)
print(man3)
print(man4)
print(man5)
print('______________Сравнение обьектов____________________________________')
for i in range(len(correct_list_man)):
    if i != len(correct_list_man)-1:
        print(correct_list_man[i] < correct_list_man[i+1])
    else:
        print(correct_list_man[i] < correct_list_man[0])

print('______________ with setter ____________________________________')

man1.age = 32
print('Changed age man1 = ', man1.age)
print('Changed year of birth man1 =', man1.year_of_birth)

man5.age = 10
print('Changed age man5 = ', man5.age)
print('Changed year of birth man5 = ', man5.year_of_birth)