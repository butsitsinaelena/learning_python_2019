import time
from multiprocessing import Process, RLock, Value, current_process


def change_number(number, some_number, global_counter):
    local_counter = 0
    while int(time.time() - start) < 10:
        some_number.value = number
        with global_counter.get_lock():
            global_counter.value += 1
        local_counter += 1
    print('Local counter for {} = {} '.format(current_process().name, local_counter))


if __name__ == '__main__':
    start = time.time()
    lock = RLock()
    N = int(input('введите число процессов: '))
    processes = []
    some_number = Value('i', -1)
    global_counter = Value('i', 0)

    for i in range(N):
        process = Process(target=change_number, args=(i, some_number, global_counter,))
        process.start()
        processes.append(process)

    while int(time.time() - start) < 10:

        print('Changing some_number = {} '.format(some_number.value))
        time.sleep(0.1)

    for process in processes:
        process.join()

    print('Global counter for processing = ', global_counter.value)
    print('Total calculation time in seconds: {}'.format(int(time.time() - start)))
