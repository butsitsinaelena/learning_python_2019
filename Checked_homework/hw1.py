N = input("enter N: ")
N = int(N)

if N > 0:
    step = 1
elif N <= 0:
    step = -1

for i in range(1, N + step, step):

    if i == 0:
        print(0)
    elif i % 5 == 0 and i % 3 == 0:
        print('FizzBuzz')
    elif i % 5 == 0:
        print('Buzz')
    elif i % 3 == 0:
        print('Fizz')
    else:
        print(i)


