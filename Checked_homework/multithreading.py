"""1. Реализовать многопоточную программу, в которой несколько (N) потоков периодически через случайное время без
   всякой синхронизации между собой меняют одну общую переменную (присваивают ей свой номер), а главный процесс
   10 раз в секунду выводит эту переменную на экран. Через некоторое время (t, например, 10 секунд)
   все потоки завершают работу.
"""

import threading
import time


def change_number(number):
    global n
    while int(time.time() - start) < 10:
        time.sleep(0.01)
        n = number


N = int(input('введите число потоков: '))
start = time.time()
threads = []
n = -1

for i in range(N):
    thr = threading.Thread(target=change_number, args=(i,))
    thr.start()
    threads.append(thr)

while int(time.time() - start) < 10:
    time.sleep(0.1)
    print('n global = ', n)

for thr in threads:
    thr.join()

print('Общее время вычислений в секундах: {}'.format(int(time.time() - start)))

