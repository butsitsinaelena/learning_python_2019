import threading
import time


class MyThread(threading.Thread):
    some_number = -1
    mutex = threading.RLock()
    global_counter = 0

    def __init__(self, thread_id):
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.local_counter = 0

    def change_some_number(self):
        MyThread.some_number = self.thread_id

    def run(self):
        while int(time.time() - start) < 10:
            time.sleep(0.01)
            with MyThread.mutex:
                self.change_some_number()
                self.local_counter += 1
                MyThread.global_counter += 1
        print('Local counter for threading №{} = {} '.format(self.thread_id, self.local_counter))


N = int(input('введите число потоков: '))
start = time.time()
threads = []
num = 0

for i in range(N):
    thread = MyThread(i)
    thread.start()
    threads.append(thread)

while int(time.time() - start) < 10:
    num += 1
    print('{} changing some_number = {} '.format(num, MyThread.some_number))
    time.sleep(0.1)

for thr in threads:
    thr.join()

print('MyThread.global_counter= ', MyThread.global_counter)
print('Общее время вычислений в секундах: {}'.format(int(time.time() - start)))