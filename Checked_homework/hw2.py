print('Задание 1')
import math
task_description = '''Теорема Пифагора: c**2 = a**2 + b**2 \nВычиcлить гипотенузу\n'''
print(task_description)
while True:
    print('Введите катет а =')
    a = float(input())
    if a <= 0:
        print('катет не может быть отрицательным числом или 0')
        continue
    break
while True:
    print('Введите катет b =')
    b = float(input())
    if b <= 0:
        print('катет не может быть отрицательным числом или 0')
        continue
    break

c = math.sqrt(a**2 + b**2)

print('Гипотенуза c равна {}'.format(c))

######################################################################

print('\nЗадание 2')

while True:
    print('Введите пятизначное число :')
    list_of_number = str(int(input()))
    list_len = len(list_of_number)
    if list_len != 5:
        print('Вы ввели не пятизначное число')
        continue
    break
list_of_number = list(map(int, list_of_number))

for i in range(list_len):
    print(i+1, 'цифра равна', list_of_number[i])
print()

######################################################################

print('\n Задание 3')

print('''Отсортировать список в порядке возрастания чисел\n''')
len_arr = int(input('Enter number:', ))
arr = []
for i in range(len_arr):
    arr_element = float(input('Enter the number of elements in the list:  '))
    arr.append(arr_element)

print('список arr = ', arr)
for i in range(len_arr):
    j = arr.index(min(arr[i:]))
    arr[i], arr[j] = arr[j], arr[i]

print('Отсортированный список arr =', arr)
