"""Написать генератор для построчного чтения файла."""

import time


def my_generator(file_name):
    with open(file_name) as f:
        lines = f.readlines()
    for line in lines:
        yield line


start = time.time()
text_name = 'text.txt'
read_line = my_generator(text_name)
while True:
    try:
        text_line = next(read_line)
    except StopIteration:
        print("\nText is the end")
        break
    print(text_line, end='')


print('\nTotal calculation time in seconds: {}'.format(time.time()-start))
