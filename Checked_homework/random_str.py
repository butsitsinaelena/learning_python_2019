"""Написать программу, запрашивающую строку у пользователя. На экран вывести эту строку с перемешанными символами."""

import random

my_str = input('enter string  ')
list_str = list(my_str)
random.shuffle(list_str)
random_str = ''.join(list_str)
print(random_str)