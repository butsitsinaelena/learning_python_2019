import time
import random

"""
Написать класс Man, который принимает имя в конструкторе. Имеет
метод solve_task, который просто выводит "I'm not ready yet".
"""
class Man:
    def __init__(self, first_name="Unknown", last_name="Unknown"):
        self.first_name = first_name
        self.last_name = last_name

    def solve_task(self):
        print('{}, I am not ready yet'.format(self))

    def __str__(self):
        return self.first_name + ' ' + self.last_name

man1 = Man('Jakob', 'Fisher')
man1.solve_task()
man2 = Man('Kitty', 'Сat')
man2.solve_task()

"""Написать класс Pupil, у которого переопределен метод solve_task. На
этот раз он будет думать от 3 до 6 секунд (c помощью метода sleep
библиотеки time и randint библиотеки random)."""

class Pupil(Man):
    def __init__(self, first_name, last_name):
        super().__init__(first_name, last_name)

    def solve_task(self):
        t = random.randint(3, 7)
        print('waiting =', t)
        time.sleep(t)
        print('{}, I am not ready yet'.format(self))

man3 = Pupil('Alexander', 'Lipov')
man3.solve_task()