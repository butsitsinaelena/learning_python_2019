"""Реализовать функциональность, которая бы “сворачивала” и “разворачивала” символы
табуляции в файле или строке. То есть, передается на вход файл или строка, необходимо
заменить все символы табуляции на четыре пробела, либо же заменить все комбинации из
четырех символов пробела на символ табуляции."""


while True:
    tab_or_space = input('Если вы хотите заменить табуляцию на 4 пробела, наберите tab, Если вы хотите заменить 4 пробела на табуляцию, наберите space :   ')
    if tab_or_space != 'tab' and tab_or_space != 'space':
        print('Вы ввели неверное значание. Попробуйте еще раз')
        continue
    break

while True:
    string_or_file = input('Если вы хотите получать информацию из файла, наберите file, Если вы хотите получать информацию из строки, наберите string:    ')
    if string_or_file != 'file' and string_or_file != 'string':
        print('Вы ввели неверное значание. Попробуйте еще раз')
        continue
    break
print('Взяв данные из {}, вы хотите поменять {}'.format(string_or_file, tab_or_space))

if string_or_file == 'file':
    with open(input('Enter file name:  '), 'r') as file:
        replacement_text = file.read()
        print('Text: ', replacement_text)
else:
    replacement_text = input('Print a string: ')

if tab_or_space == 'tab':
    replacement_text = replacement_text.replace('\t', '    ')
    print('Text replaced with a tab by a space or replaced with a space by a tab:  ', replacement_text)
else:
    replacement_text = replacement_text.replace('    ', '\t')
    print('Text replaced with a tab by a space or replaced with a space by a tab:   ', replacement_text)


