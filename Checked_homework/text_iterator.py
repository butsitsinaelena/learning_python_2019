"""
1.Реализовать итератор, который бы “читал” заданный текст по
параграфам. Символ параграфа задается отдельно.
"""


class MyIterator:

    def __init__(self):
        with open('text.txt', 'r') as f:
            self.text = f.read()
        self.paragraph_start = 0
        self.paragraph_end = 0
        self.paragraph_end_symbol = input('Enter end paragraph symbol:\n ')

    def __iter__(self):
        return self

    def __next__(self):
        if self.paragraph_end != -1:
            self.paragraph_start = self.paragraph_end
            self.paragraph_end = self.text.find(self.paragraph_end_symbol, self.paragraph_start+1)
            self.current_paragraph = self.text[self.paragraph_start: self.paragraph_end]
            return self.current_paragraph
        else:
            raise StopIteration


itr = MyIterator()
while True:
    try:
        paragraph = next(itr)
    except StopIteration:
        print("\nText is the end")
        break
    print(paragraph)

