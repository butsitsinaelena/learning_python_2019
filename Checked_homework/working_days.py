""" Написать функцию для подсчета количества рабочих дней между двумя датами (даты передаются в качестве параметров).
    Можно для простоты считать нерабочими все субботы и воскресенья."""

from datetime import date, timedelta


def count_working_days(first_date, second_date):
    difference_between_dates = (second_date - first_date).days + 1
    workday = 0
    for i in range(difference_between_dates):
        day = first_date + timedelta(days=i)
        if day.isoweekday() < 6:
            workday += 1
    return workday


date1 = date(2019, 10, 12)
date2 = date(2019, 10, 27)
print('first date = ',  date1)
print('second date = ', date2)
print('workday = ', count_working_days(date1, date2))


# # пока полежит : ввод через input, ограничения не предусмотрены
# date1 = input('first date (гггг-мм-дд): ')
# date2 = input('second date (гггг-мм-дд): ')
# date1 = date1.split('-')
# date2 = date2.split('-')
# date1 = date(int(date1[0]), int(date1[1]), int(date1[2]))
# date2 = date(int(date2[0]), int(date2[1]), int(date2[2]))
# print('first date = ',  date1)
# print('second date = ', date2)
# print('workday = ', count_working_days(date1, date2))