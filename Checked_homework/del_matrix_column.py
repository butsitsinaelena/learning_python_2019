""" * Есть список списков (матрица). Каждый внутренний список - это строка матрицы.
Необходимо реализовать функцию, которая удаляет столбец, который содержит заданную
цифру
"""
matrix = [[1, 2, 3, 4, 5],
          [5, 4, 3, 2, 1],
          [6, 6, 6, 6, 6],
          [7, 8, 9, 0, 10]]


def search_index(matrix):
    """ функция ищет индекс столбца, который содержит заданную цифру
    :param matrix: матрица, в которой ведется поиск столбца
    :return: индекс столбца, который содержит заданную цифру, или None - если такого столбца нет
    """
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if matrix[i][j] == number_to_delete:
                return j
    return None


def del_column(matrix, column_del):
    """ функция, которая удаляет столбец содержащий заданную цифру
    :param matrix: матрица в которой будет удален столбец
    :param column_del: номер столбца, который нужно удалить
    :return: матрица с удаленным столбцом
    """
    j = column_del
    for i in range(len(matrix)):
        del matrix[i][j]
    return matrix


def print_matrix(matrix):
     """ Функция, которая красиво печатает матрицу"""
     for row in matrix:
         for elem in row:
             print(elem, end=' ')
         print()
     return


print_matrix(matrix)
number_to_delete = float(input(' Удалить столбец с цифрой :  '))

while True:
    column_del = search_index(matrix)
    if column_del == None:
        break
    else:
        matrix = del_column(matrix, column_del)

print('matrix after del column')
print_matrix(matrix)


