"""1. Реализовать многопоточную программу, в которой несколько (N) потоков периодически через случайное время без
   всякой синхронизации между собой меняют одну общую переменную (присваивают ей свой номер), а главный процесс
   10 раз в секунду выводит эту переменную на экран. Через некоторое время (t, например, 10 секунд)
   все потоки завершают работу.
   2. Добавить в программу еще одну переменную-счетчик количества обращений к общей
   переменной. При этом, каждый процесс, меняя общую переменную увеличивает общий
   счетчик на 1, а так же хранит у себя локально свой собственный счетчик оращений.
   При завершении каждый дочерний поток выводит свой счетчик на экран, а родительский
   поток - общий счетчик.
"""

import threading
import time


def change_number(number):
    global some_number
    global global_counter
    local_counter = 0
    while int(time.time() - start) < 10:
        time.sleep(0.01)
        some_number = number
        local_counter += 1
        global_counter += 1
    print('Local counter for threading №{} = {} '. format(number, local_counter))


N = int(input('Enter threading number: '))
start = time.time()
threads = []
some_number = -1
global_counter = 0
num = 0

for i in range(N):
    thr = threading.Thread(target=change_number, args=(i,))
    thr.start()
    threads.append(thr)

while int(time.time() - start) < 10:
    num += 1
    print('{} changing some_number = {} '.format(num, some_number))
    time.sleep(0.1)

for thr in threads:
    thr.join()

print('Global counter for threading = ', global_counter)
print('Total calculation time in seconds: {}'.format(int(time.time() - start)))
